package com.haizi.battery.entity.battery;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 电池列表详情
 */
@Data
@TableName("JSON_DATA")
public class BatteryLogDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * VIN
     */
    private String vin;

    /**
     * 数据时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dataTime;

    /**
     * 整机运行数据
     */
    private String allParameters;

    /**
     * 储能装置电压电流数据
     */
    private String energyStorage;

    /**
     * 电池箱体电压数据
     */
    private String batteryBodyVoltage;

    /**
     * 电池箱体温度数据
     */
    private String batteryBodyTemperature;

    /**
     * 电池箱运行状态
     */
    private String batteryBodyRunStatus;

    /**
     * 电池箱容量参数
     */
    private String batteryBodyCapacity;

    /**
     * 从机通信状态
     */
    private String slaveCommunicationStatus;

    /**
     * 整机故障状态
     */
    private String allFault;

    /**
     * EMS-To-PCS 参数
     */
    @TableField(value = "ems_2_pcs")
    private String ems2Pcs;

    /**
     * 电池包个数
     */
    private Short batteryCount;

    /**
     * 电池总容量
     */
    private Long batteryAllCapacity;

    /**
     * SOC 单位：%，有效值范围：0%-100%
     */
    private Integer soc;

    /**
     * 告警情况
     */
    private Short alarm;

    /**
     * 定位状态
     */
    private Short locationStatus;

    public BatteryLogDetail() {}

    public BatteryLogDetail(RealTimeData dataTime) {
        this.vin = dataTime.getVin();
        this.dataTime =  dataTime.getDataTime();
        this.allParameters = dataTime.getAllParameters();
        this.energyStorage = dataTime.getEnergyStorage();
        this.batteryBodyVoltage = dataTime.getBatteryBodyVoltage();
        this.batteryBodyTemperature = dataTime.getBatteryBodyTemperature();
        this.batteryBodyRunStatus = dataTime.getBatteryBodyRunStatus();
        this.batteryBodyCapacity = dataTime.getBatteryBodyCapacity();
        this.slaveCommunicationStatus = dataTime.getSlaveCommunicationStatus();
        this.allFault = dataTime.getAllFault();
        this.ems2Pcs = dataTime.getEms2Pcs();
        if (dataTime.getLongitude() != null
                && dataTime.getLongitude().intValue() != 0) {
            this.locationStatus = 0;
        }
        this.alarm = dataTime.getAlarm();
        this.soc = dataTime.getSoc();
        this.batteryAllCapacity = dataTime.getBatteryAllCapacity();
        this.batteryCount = dataTime.getBatteryCount();
    }

}
