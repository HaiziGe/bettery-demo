package com.haizi.battery.entity.battery;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 电池
 *
 * @author HaiziGe
 * @date 2020-07-12 13:50:02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("battery")
public class Battery extends Model<Battery> {
    private static final long serialVersionUID = 1L;

    /**
     * 设备id
     */
    @TableId(value = "battery_id", type = IdType.AUTO)
    private Integer batteryId;

    /**
     * 设备编号
     */
    private String batteryNo;

    /**
     * 电池容量和规格
     */
    private String batteryCapacity;

    /**
     * iccid
     */
    private String iccid;

    /**
     * imei
     */
    private String imei;

    /**
     * 电池型号
     */
    private String batteryType;

    /**
     * 电压
     */
    private Float voltage;

    /**
     * 电流
     */
    private Float electricCurrent;

    /**
     * 温度
     */
    private Integer temperature;

    /**
     * 健康状态
     */
    private String health;

    /**
     * SOC
     */
    private Integer soc;

    /**
     * 经度
     */
    private BigDecimal lng;

    /**
     * 纬度
     */
    private BigDecimal lat;

    /**
     * 状态
     */
    private String status;

    /**
     * 告警信息
     */
    private String alarm;

    /**
     * 投运时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date putTime;

    /**
     * 数据时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dataTime;

    /**
     * 运行状态
     */
    private String runStatus;

    /**
     * 电池组数据
     */
    private String batteryVoltageList;

}
