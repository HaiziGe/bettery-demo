package com.haizi.battery.entity.battery;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.haizi.battery.gate.core.evgb.entity.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.util.Date;
import java.util.OptionalDouble;

/**
 * 储能站
 *
 * @author HaiziGe
 * @date 2020-07-27 16:19:04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("REAL_TIME_DATA")
public class RealTimeData extends Model<RealTimeData> {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * VIN
     */
    private String vin;

    /**
     * nmae
     */
    private String name;

    /**
     * 数据时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dataTime;

    /**
     * 运行模式
     */
    private Short runModel;

    /**
     * 电池包个数
     */
    private Short batteryCount;

    /**
     * 电池总容量
     */
    private Long batteryAllCapacity;

    /**
     * SOC 单位：%，有效值范围：0%-100%
     */
    private Integer soc;

    /**
     * 充电温度
     */
    private Short chargeTemperature;

    /**
     * 放电温度
     */
    private Short dischargeTemperature;

    /**
     * 交流电压给定
     */
    private Integer acVoltageGive;

    /**
     * 有功功率给定
     */
    private Integer acActivePowerGive;

    /**
     * 无功功率给定
     */
    private Integer acReactivePowerGive;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    /**
     * 在线状态 0在线 1不在线
     */
    private Short online;

    /**
     * 告警情况
     */
    private Short alarm;

    /**
     * 整机运行数据
     */
    private String allParameters;

    /**
     * 储能装置电压电流数据
     */
    private String energyStorage;

    /**
     * 电池箱体电压数据
     */
    private String batteryBodyVoltage;

    /**
     * 电池箱体温度数据
     */
    private String batteryBodyTemperature;

    /**
     * 电池箱运行状态
     */
    private String batteryBodyRunStatus;

    /**
     * 电池箱容量参数
     */
    private String batteryBodyCapacity;

    /**
     * 从机通信状态
     */
    private String slaveCommunicationStatus;

    /**
     * 整机故障状态
     */
    private String allFault;

    /**
     * EMS-To-PCS 参数
     */
    @TableField(value = "ems_2_pcs")
    private String ems2Pcs;


    public RealTimeData() {}

    public RealTimeData(String vin, RealTime realTime) {
        this.vin = vin;
        this.dataTime = realTime.getBeanTime().getDateTime();
        this.runModel = realTime.getAllParameters().getRunModel();
        this.acVoltageGive = realTime.getEms2Pcs().getAcVoltageGive();
        this.acActivePowerGive = realTime.getEms2Pcs().getAcActivePowerGive();
        this.acReactivePowerGive = realTime.getEms2Pcs().getAcReactivePowerGive();
        this.longitude = BigDecimal.valueOf(realTime.getLocationData().getLongitude()).multiply(new BigDecimal("0.000001"));
        this.latitude = BigDecimal.valueOf(realTime.getLocationData().getLatitude()).multiply(new BigDecimal("0.000001"));
        this.alarm = realTime.getAllFault().getAlarm();
        this.batteryCount = realTime.getBatteryBodyCapacity().getBodyCount();
        this.batteryAllCapacity = 0L;
        if (this.batteryCount > 0) {
            this.batteryAllCapacity = realTime.getBatteryBodyCapacity().getBatteryCapacityList().parallelStream().mapToLong(BatteryCapacity::getRemainingCapacity).sum();
            OptionalDouble average = realTime.getBatteryBodyRunStatus().getBatteryRunStatusList().parallelStream().mapToInt(BatteryRunStatus::getSoc).average();
            if (average.isPresent()) {
                this.soc = ((Double)average.getAsDouble()).intValue();
            } else {
                this.soc = 0;
            }
        }
        this.allParameters = JSON.toJSONString(realTime.getAllParameters());
        this.energyStorage = JSON.toJSONString(realTime.getEnergyStorage());
        this.batteryBodyVoltage = JSON.toJSONString(realTime.getBatteryBodyVoltage());
        this.batteryBodyTemperature = JSON.toJSONString(realTime.getBatteryBodyTemperature());
        this.batteryBodyRunStatus = JSON.toJSONString(realTime.getBatteryBodyRunStatus());
        this.batteryBodyCapacity = JSON.toJSONString(realTime.getBatteryBodyCapacity());
        this.slaveCommunicationStatus = JSON.toJSONString(realTime.getSlaveCommunicationStatus());
        this.allFault = JSON.toJSONString(realTime.getAllFault());
        this.ems2Pcs = JSON.toJSONString(realTime.getEms2Pcs());
    }

}
