package com.haizi.battery.entity.battery;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 电池历史数据
 *
 * @author HaiziGe
 * @date 2020-07-12 13:50:02
 */
@Data
public class BatteryHistory {
    private static final long serialVersionUID = 1L;

    /**
     * 设备id
     */
    private Battery battery;

    /**
     * 电压数据列表
     */
    private List<String> voltageList;

    /**
     * 电流数据列表
     */
    private List<String> electricCurrentList;

    /**
     * 温度系列
     */
    private List<String> temperatureList;

    /**
     * 时间序列
     */
    private List<String> dateList;
}
