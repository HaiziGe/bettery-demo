package com.haizi.battery.entity.busi.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * 微信用户表
 *
 * @author Haizi
 * @date 2019-08-13 19:34:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("app_user")
public class AppUser extends Model<AppUser> {
    private static final long serialVersionUID = 1L;

    /**
     * 小程序用户id
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    /**
     * 唯一标识 重要
     */
    private String openid;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 市
     */
    private String city;

    /**
     * 国
     */
    private String country;

    /**
     * 省
     */
    private String province;

    /**
     * 登陆认证 重要
     */
    private String sessionkey;

    /**
     * 1男2女
     */
    private String gender;

    /**
     * 用户昵称
     */
    private String userNickName;

    /**
     * 用户昵称
     */
    private String avatarUrl;

    /**
     * 用户类型0普通用户 1代理商 2店家
     */
    private Integer userType;

    /**
     * 当为代理商/店家时，代理商的id
     */
    private Integer deptId;

    /**
     * 用户自己设置的名字
     */
    private String userName;

    /**
     * 身高
     */
    private Integer userHeight;

    /**
     * 体重
     */
    private Integer userWeight;

    /**
     * 用户生日
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date userBirthday;

    /**
     * 用户性别
     */
    private Integer userGender;

    /**
     * 用户自己设置的手机号
     */
    private String userMobile;

    /**
     * 用户想拥有服务类型，逗号隔开的多项选择
     */
    private String userServiceType;

    /**
     * 用户所在地，基本地址
     */
    private String userServiceAddress;

    /**
     * 首次扫码的设备商-谁引流进来的
     */
    private Integer fromDeptId;

    /**
     * 用户管理单位
     */
    @TableField(exist = false)
    private String deptName;

    /**
     * 与用户相关的代理商
     */
    @TableField(exist = false)
    private List<Integer> deptIdList;
}
