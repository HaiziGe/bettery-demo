package com.haizi.battery;

import com.haizi.battery.gate.server.config.EvGBProperties;
import com.haizi.battery.gate.server.netty.EvGBServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * @author haizi
 * @date 2019-07-21 18:41:23
 *
 */
@SpringBootApplication
@EnableConfigurationProperties(value = {EvGBProperties.class})
public class WildAssApplication {

    public static void main(String[] args) {
        SpringApplication.run(WildAssApplication.class, args);
    }

    @Autowired
    private EvGBProperties evGBProperties;

    @Bean
    public EvGBServer server() {
        EvGBServer server = new EvGBServer();
        server.setPort(evGBProperties.getPort());
        server.setTimeout(evGBProperties.getTimeout());
        server.setSoBackLog(evGBProperties.getSoBackLog());
        return server;
    }

}
