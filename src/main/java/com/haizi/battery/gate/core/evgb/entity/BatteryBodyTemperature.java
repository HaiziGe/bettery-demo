package com.haizi.battery.gate.core.evgb.entity;

import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.base.IStatus;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class BatteryBodyTemperature implements IStatus {

    /**
     * 电池箱体个数
     */
    private Short bodyCount;

    /**
     * 报警通用标志
     */
    private List<BatteryTemperature> batteryTemperatureList;

    @Override
    public BatteryBodyTemperature decode(ByteBuf byteBuf) throws BaseException {
        BatteryBodyTemperature batteryBodyTemperature = new BatteryBodyTemperature();
        batteryBodyTemperature.setBodyCount(byteBuf.readUnsignedByte());
        List<BatteryTemperature> batteryTemperatureList =new ArrayList<>();
        for (int i = 0; i <batteryBodyTemperature.getBodyCount(); i++) {
            BatteryTemperature batteryTemperature = new BatteryTemperature();
            batteryTemperature.setBodyNo(byteBuf.readUnsignedByte());
            batteryTemperature.setSensorCount(byteBuf.readUnsignedByte());
            if (batteryTemperature.getSensorCount() > 0) {
                batteryTemperature.setTemperatureList(new ArrayList<>());
            }
            for (int j = 0; j < batteryTemperature.getSensorCount(); j++) {
                batteryTemperature.getTemperatureList().add(byteBuf.readUnsignedByte());
            }
            batteryTemperatureList.add(batteryTemperature);
        }
        batteryBodyTemperature.setBatteryTemperatureList(batteryTemperatureList);
        return batteryBodyTemperature;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.order(ByteOrder.BIG_ENDIAN);
        return buffer;
    }

    public Short getBodyCount() {
        return bodyCount;
    }

    public void setBodyCount(Short bodyCount) {
        this.bodyCount = bodyCount;
    }

    public List<BatteryTemperature> getBatteryTemperatureList() {
        return batteryTemperatureList;
    }

    public void setBatteryTemperatureList(List<BatteryTemperature> batteryTemperatureList) {
        this.batteryTemperatureList = batteryTemperatureList;
    }
}
