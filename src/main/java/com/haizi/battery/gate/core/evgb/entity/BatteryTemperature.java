package com.haizi.battery.gate.core.evgb.entity;

import java.util.List;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class BatteryTemperature {

    /**
     * 电池箱体编号
     */
    private Short bodyNo;

    /**
     * 传感器个数
     */
    private Short sensorCount;

    /**
     * 温度列表
     */
    private List<Short> temperatureList;

    public Short getBodyNo() {
        return bodyNo;
    }

    public void setBodyNo(Short bodyNo) {
        this.bodyNo = bodyNo;
    }

    public Short getSensorCount() {
        return sensorCount;
    }

    public void setSensorCount(Short sensorCount) {
        this.sensorCount = sensorCount;
    }

    public List<Short> getTemperatureList() {
        return temperatureList;
    }

    public void setTemperatureList(List<Short> temperatureList) {
        this.temperatureList = temperatureList;
    }

}
