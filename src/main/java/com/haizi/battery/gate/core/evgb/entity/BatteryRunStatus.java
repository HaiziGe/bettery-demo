package com.haizi.battery.gate.core.evgb.entity;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class BatteryRunStatus {

    /**
     * 电池箱体编号 有效值范围：1 ～ 255
     */
    private Short bodyNo;
    /**
     * 放电状态 1-放电，0-无
     */
    private Short dischargeSatus;
    /**
     * 充电状态 1-充电，0-无
     */
    private Short chargeSatus;
    /**
     * MOS 温度状态 1-有，0-无
     */
    private Short mosTempSatus;
    /**
     * 环境温度状态 1-有，0-无
     */
    private Short envTempSatus;
    /**
     * 无符号电流值  单位：mA，精度 10mA/bit
     */
    private Integer current;
    /**
     * 电芯过压保护状态 1-有，0-无
     */
    private Short cellOverVoltageProtection;
    /**
     * 总压过压保护状态 1-有，0-无
     */
    private Short allOverVoltageProtection;
    /**
     * 充满保护状态 1-有，0-无
     */
    private Short fullProtection;
    /**
     * 电芯欠压保护状态 1-有，0-无
     */
    private Short cellUnderVoltageProtection;
    /**
     * 总压欠压保护状态 1-有，0-无
     */
    private Short allUnderVoltageProtection;
    /**
     * 充电温度保护 1-有，0-无
     */
    private Short chargeTempProtection;
    /**
     * 放电温度保护 1-有，0-无
     */
    private Short dischargeTempProtection;
    /**
     * MOS 过温保护 1-有，0-无
     */
    private Short mosOverTempProtection;
    /**
     * 高温保护状态 1-有，0-无
     */
    private Short overTempProtection;
    /**
     * 低温保护状态 1-有，0-无
     */
    private Short underTempProtection;
    /**
     * 放电短路保护 1-有，0-无
     */
    private Short dischargeShortCircuitProtection;
    /**
     * 放电过流保护 1-有，0-无
     */
    private Short dischargeOverCurrentProtection;
    /**
     * 充电过流保护 1-有，0-无
     */
    private Short chargeOverCurrentProtection;
    /**
     * 环境高温保护 1-有，0-无
     */
    private Short envHighTempProtection;
    /**
     * 环境低温保护 1-有，0-无
     */
    private Short envLowTempProtection;
    /**
     * 放电 MOS 1-开，0-关
     */
    private Short dischargeMos;
    /**
     * 充电 MOS 1-开，0-关
     */
    private Short chargeMos;
    /**
     * 温度采集失效 1-失效，0-无
     */
    private Short cellectTempFail;
    /**
     * 电压采集失效 1-失效，0-无
     */
    private Short cellectVoltageFail;
    /**
     * 放电 MOS 1-失效，0-无
     */
    private Short dischargeMos1;

    /**
     * 充电 MOS 1-失效，0-无
     */
    private Short chargeMos1;
    /**
     * SOC 单位：%，有效值范围：0%-100%
     */
    private Short soc;

    /**
     * 循环次数
     */
    private Integer loopCount;

    public Short getBodyNo() {
        return bodyNo;
    }

    public void setBodyNo(Short bodyNo) {
        this.bodyNo = bodyNo;
    }

    public Short getDischargeSatus() {
        return dischargeSatus;
    }

    public void setDischargeSatus(Short dischargeSatus) {
        this.dischargeSatus = dischargeSatus;
    }

    public Short getChargeSatus() {
        return chargeSatus;
    }

    public void setChargeSatus(Short chargeSatus) {
        this.chargeSatus = chargeSatus;
    }

    public Short getMosTempSatus() {
        return mosTempSatus;
    }

    public void setMosTempSatus(Short mosTempSatus) {
        this.mosTempSatus = mosTempSatus;
    }

    public Short getEnvTempSatus() {
        return envTempSatus;
    }

    public void setEnvTempSatus(Short envTempSatus) {
        this.envTempSatus = envTempSatus;
    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Short getCellOverVoltageProtection() {
        return cellOverVoltageProtection;
    }

    public void setCellOverVoltageProtection(Short cellOverVoltageProtection) {
        this.cellOverVoltageProtection = cellOverVoltageProtection;
    }

    public Short getAllOverVoltageProtection() {
        return allOverVoltageProtection;
    }

    public void setAllOverVoltageProtection(Short allOverVoltageProtection) {
        this.allOverVoltageProtection = allOverVoltageProtection;
    }

    public Short getFullProtection() {
        return fullProtection;
    }

    public void setFullProtection(Short fullProtection) {
        this.fullProtection = fullProtection;
    }

    public Short getCellUnderVoltageProtection() {
        return cellUnderVoltageProtection;
    }

    public void setCellUnderVoltageProtection(Short cellUnderVoltageProtection) {
        this.cellUnderVoltageProtection = cellUnderVoltageProtection;
    }

    public Short getAllUnderVoltageProtection() {
        return allUnderVoltageProtection;
    }

    public void setAllUnderVoltageProtection(Short allUnderVoltageProtection) {
        this.allUnderVoltageProtection = allUnderVoltageProtection;
    }

    public Short getChargeTempProtection() {
        return chargeTempProtection;
    }

    public void setChargeTempProtection(Short chargeTempProtection) {
        this.chargeTempProtection = chargeTempProtection;
    }

    public Short getDischargeTempProtection() {
        return dischargeTempProtection;
    }

    public void setDischargeTempProtection(Short dischargeTempProtection) {
        this.dischargeTempProtection = dischargeTempProtection;
    }

    public Short getMosOverTempProtection() {
        return mosOverTempProtection;
    }

    public void setMosOverTempProtection(Short mosOverTempProtection) {
        this.mosOverTempProtection = mosOverTempProtection;
    }

    public Short getOverTempProtection() {
        return overTempProtection;
    }

    public void setOverTempProtection(Short overTempProtection) {
        this.overTempProtection = overTempProtection;
    }

    public Short getUnderTempProtection() {
        return underTempProtection;
    }

    public void setUnderTempProtection(Short underTempProtection) {
        this.underTempProtection = underTempProtection;
    }

    public Short getDischargeShortCircuitProtection() {
        return dischargeShortCircuitProtection;
    }

    public void setDischargeShortCircuitProtection(Short dischargeShortCircuitProtection) {
        this.dischargeShortCircuitProtection = dischargeShortCircuitProtection;
    }

    public Short getDischargeOverCurrentProtection() {
        return dischargeOverCurrentProtection;
    }

    public void setDischargeOverCurrentProtection(Short dischargeOverCurrentProtection) {
        this.dischargeOverCurrentProtection = dischargeOverCurrentProtection;
    }

    public Short getChargeOverCurrentProtection() {
        return chargeOverCurrentProtection;
    }

    public void setChargeOverCurrentProtection(Short chargeOverCurrentProtection) {
        this.chargeOverCurrentProtection = chargeOverCurrentProtection;
    }

    public Short getEnvHighTempProtection() {
        return envHighTempProtection;
    }

    public void setEnvHighTempProtection(Short envHighTempProtection) {
        this.envHighTempProtection = envHighTempProtection;
    }

    public Short getEnvLowTempProtection() {
        return envLowTempProtection;
    }

    public void setEnvLowTempProtection(Short envLowTempProtection) {
        this.envLowTempProtection = envLowTempProtection;
    }

    public Short getDischargeMos() {
        return dischargeMos;
    }

    public void setDischargeMos(Short dischargeMos) {
        this.dischargeMos = dischargeMos;
    }

    public Short getChargeMos() {
        return chargeMos;
    }

    public void setChargeMos(Short chargeMos) {
        this.chargeMos = chargeMos;
    }

    public Short getCellectTempFail() {
        return cellectTempFail;
    }

    public void setCellectTempFail(Short cellectTempFail) {
        this.cellectTempFail = cellectTempFail;
    }

    public Short getCellectVoltageFail() {
        return cellectVoltageFail;
    }

    public void setCellectVoltageFail(Short cellectVoltageFail) {
        this.cellectVoltageFail = cellectVoltageFail;
    }

    public Short getDischargeMos1() {
        return dischargeMos1;
    }

    public void setDischargeMos1(Short dischargeMos1) {
        this.dischargeMos1 = dischargeMos1;
    }

    public Short getChargeMos1() {
        return chargeMos1;
    }

    public void setChargeMos1(Short chargeMos1) {
        this.chargeMos1 = chargeMos1;
    }

    public Short getSoc() {
        return soc;
    }

    public void setSoc(Short soc) {
        this.soc = soc;
    }

    public Integer getLoopCount() {
        return loopCount;
    }

    public void setLoopCount(Integer loopCount) {
        this.loopCount = loopCount;
    }

}
