package com.haizi.battery.gate.core.evgb.entity;

import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.base.IStatus;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class EnergyStorage implements IStatus {

    /**
     * 电网电压有效值 单位 V，精度 0.1
     */
    private Integer gridVoltageEft;
    /**
     * 电网电流有效值 单位 A，精度 0.1
     */
    private Integer gridCurrentEft;
    /**
     * 负载电压有效值 单位 V，精度 0.1
     */
    private Integer loadVoltageEft;
    /**
     * 负载电流有效值 单位 A，精度 0.1
     */
    private Integer loadCurrentEft;
    /**
     * 逆变电压有效值 单位 V，精度 0.1
     */
    private Integer invertVoltageEft;
    /**
     * 逆变电流有效值 单位 A，精度 0.1
     */
    private Integer invertCurrentEft;
    /**
     * 电网电压直流值 单位 V，精度 0.1
     */
    private Integer gridVoltageDc;
    /**
     * 电网电流直流值 单位 A，精度 0.1
     */
    private Integer gridCurrentDc;
    /**
     * 负载电压直流值 单位 V，精度 0.1
     */
    private Integer loadVoltageDc;
    /**
     * 负载电流直流值 单位 A，精度 0.1
     */
    private Integer loadCurrentDc;
    /**
     * 逆变电压直流值 单位 V，精度 0.1
     */
    private Integer invertVoltageDc;
    /**
     * 逆变电流直流值 单位 A，精度 0.1
     */
    private Integer invertCurrentDc;
    /**
     * BUS 电压直流值 单位 V，精度 0.1
     */
    private Integer busVoltageDc;
    /**
     * BUS 电流直流值 单位 A，精度 0.1
     */
    private Integer busCurrentDc;
    /**
     * BAT 电压直流值 单位 V，精度 0.1
     */
    private Integer batVoltageDc;

    /**
     * BAT 电流直流值 单位 A，精度 0.1
     */
    private Integer batCurrentDc;

    @Override
    public EnergyStorage decode(ByteBuf byteBuf) throws BaseException {
        EnergyStorage energyStorage = new EnergyStorage();
        energyStorage.setGridVoltageEft((int) byteBuf.readUnsignedShort());
        energyStorage.setGridCurrentEft((int) byteBuf.readUnsignedShort());
        energyStorage.setLoadVoltageEft((int) byteBuf.readUnsignedShort());
        energyStorage.setLoadCurrentEft((int) byteBuf.readUnsignedShort());
        energyStorage.setInvertVoltageEft((int) byteBuf.readUnsignedShort());
        energyStorage.setInvertCurrentEft((int) byteBuf.readUnsignedShort());
        energyStorage.setGridVoltageDc((int) byteBuf.readUnsignedShort());
        energyStorage.setGridCurrentDc((int) byteBuf.readUnsignedShort());
        energyStorage.setLoadVoltageDc((int) byteBuf.readUnsignedShort());
        energyStorage.setLoadCurrentDc((int) byteBuf.readUnsignedShort());
        energyStorage.setInvertVoltageDc((int) byteBuf.readUnsignedShort());
        energyStorage.setInvertCurrentDc((int) byteBuf.readUnsignedShort());
        energyStorage.setBusVoltageDc((int) byteBuf.readUnsignedShort());
        energyStorage.setBusCurrentDc((int) byteBuf.readUnsignedShort());
        energyStorage.setBatVoltageDc((int) byteBuf.readUnsignedShort());
        energyStorage.setBatCurrentDc((int) byteBuf.readUnsignedShort());
        return energyStorage;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.order(ByteOrder.BIG_ENDIAN);

        return buffer;
    }


    public Integer getGridVoltageEft() {
        return gridVoltageEft;
    }

    public void setGridVoltageEft(Integer gridVoltageEft) {
        this.gridVoltageEft = gridVoltageEft;
    }

    public Integer getGridCurrentEft() {
        return gridCurrentEft;
    }

    public void setGridCurrentEft(Integer gridCurrentEft) {
        this.gridCurrentEft = gridCurrentEft;
    }

    public Integer getLoadVoltageEft() {
        return loadVoltageEft;
    }

    public void setLoadVoltageEft(Integer loadVoltageEft) {
        this.loadVoltageEft = loadVoltageEft;
    }

    public Integer getLoadCurrentEft() {
        return loadCurrentEft;
    }

    public void setLoadCurrentEft(Integer loadCurrentEft) {
        this.loadCurrentEft = loadCurrentEft;
    }

    public Integer getInvertVoltageEft() {
        return invertVoltageEft;
    }

    public void setInvertVoltageEft(Integer invertVoltageEft) {
        this.invertVoltageEft = invertVoltageEft;
    }

    public Integer getInvertCurrentEft() {
        return invertCurrentEft;
    }

    public void setInvertCurrentEft(Integer invertCurrentEft) {
        this.invertCurrentEft = invertCurrentEft;
    }

    public Integer getGridVoltageDc() {
        return gridVoltageDc;
    }

    public void setGridVoltageDc(Integer gridVoltageDc) {
        this.gridVoltageDc = gridVoltageDc;
    }

    public Integer getGridCurrentDc() {
        return gridCurrentDc;
    }

    public void setGridCurrentDc(Integer gridCurrentDc) {
        this.gridCurrentDc = gridCurrentDc;
    }

    public Integer getLoadVoltageDc() {
        return loadVoltageDc;
    }

    public void setLoadVoltageDc(Integer loadVoltageDc) {
        this.loadVoltageDc = loadVoltageDc;
    }

    public Integer getLoadCurrentDc() {
        return loadCurrentDc;
    }

    public void setLoadCurrentDc(Integer loadCurrentDc) {
        this.loadCurrentDc = loadCurrentDc;
    }

    public Integer getInvertVoltageDc() {
        return invertVoltageDc;
    }

    public void setInvertVoltageDc(Integer invertVoltageDc) {
        this.invertVoltageDc = invertVoltageDc;
    }

    public Integer getInvertCurrentDc() {
        return invertCurrentDc;
    }

    public void setInvertCurrentDc(Integer invertCurrentDc) {
        this.invertCurrentDc = invertCurrentDc;
    }

    public Integer getBusVoltageDc() {
        return busVoltageDc;
    }

    public void setBusVoltageDc(Integer busVoltageDc) {
        this.busVoltageDc = busVoltageDc;
    }

    public Integer getBusCurrentDc() {
        return busCurrentDc;
    }

    public void setBusCurrentDc(Integer busCurrentDc) {
        this.busCurrentDc = busCurrentDc;
    }

    public Integer getBatVoltageDc() {
        return batVoltageDc;
    }

    public void setBatVoltageDc(Integer batVoltageDc) {
        this.batVoltageDc = batVoltageDc;
    }

    public Integer getBatCurrentDc() {
        return batCurrentDc;
    }

    public void setBatCurrentDc(Integer batCurrentDc) {
        this.batCurrentDc = batCurrentDc;
    }

}
