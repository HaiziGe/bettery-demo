package com.haizi.battery.gate.core.evgb.entity;

import com.alibaba.fastjson.JSON;
import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.base.IStatus;
import com.haizi.battery.gate.core.evgb.enumtype.RealTimeDataType;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/**
 * 实时数据上报
 */
@SuppressWarnings("all")
public class RealTime implements IStatus {

    private static final Logger LOGGER = LoggerFactory.getLogger(RealTime.class);

    private static final BeanTime producer = new BeanTime();

    /**
     * 数据采集时间
     */
    private BeanTime beanTime;

    /**
     * 整车数据
     */
    private VehicleData vehicleData;

    /**
     * 驱动电机个数
     */
    private Short driveMotorCount;

    /**
     * 驱动电机数据列表
     */
    private List<DriveMotorData> driveMotorDatas;

    /**
     * 燃料电池数据
     */
    private FuelCellData fuelCellData;

    /**
     * 发动机数据
     */
    private EngineData engineData;

    /**
     * 位置数据
     */
    private LocationData locationData;

    /**
     * 极值数据
     */
    private ExtremeData extremeData;

    /**
     * 报警数据
     */
    private AlarmData alarmData;

    /**
     * 整机运行数据
     */
    private AllParameters allParameters;

    /**
     * 整机运行数据
     */
    private EnergyStorage energyStorage;

    /**
     * 整机运行数据
     */
    private BatteryBodyVoltage batteryBodyVoltage;

    /**
     * 整机运行数据
     */
    private BatteryBodyTemperature batteryBodyTemperature;

    /**
     * 整机运行数据
     */
    private BatteryBodyRunStatus batteryBodyRunStatus;

    /**
     * 整机运行数据
     */
    private BatteryBodyCapacity batteryBodyCapacity;

    /**
     * 整机运行数据
     */
    private SlaveCommunicationStatus slaveCommunicationStatus;

    /**
     * 整机运行数据
     */
    private AllFault allFault;

    /**
     * 整机运行数据
     */
    private Ems2Pcs ems2Pcs;

    /**
     * 可充电储能装置电压数据个数
     */
    private Short subsystemVoltageCount;

    /**
     * 可充电储能装置电压数据列表
     */
    private List<SubsystemVoltageData> subsystemVoltageDatas;

    /**
     * 可充电储能装置温度数据个数
     */
    private Short subsystemTemperatureCount;

    /**
     * 可充电储能装置温度数据列表
     */
    private List<SubsystemTemperatureData> subsystemTemperatureDatas;

    @Override
    public RealTime decode(ByteBuf byteBuf) throws BaseException {
        RealTime realTimeData = new RealTime();
        BeanTime beanTime = producer.decode(byteBuf);
        realTimeData.setBeanTime(beanTime);
        while (byteBuf.isReadable()) {
            decodeByType(byteBuf, realTimeData);
        }
        return realTimeData;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.writeBytes(beanTime.encode());
        if (vehicleData != null) {
            buffer.writeByte(RealTimeDataType.VEHICLE.getId());
            buffer.writeBytes(vehicleData.encode());
        }
        if (driveMotorCount > 0 && driveMotorDatas != null) {
            buffer.writeByte(RealTimeDataType.DRIVEMOTOR.getId());
            buffer.writeByte(driveMotorCount);
            for (int i = 0; i < driveMotorCount; i++) {
                buffer.writeBytes(driveMotorDatas.get(i).encode());
            }
        }
        if (fuelCellData != null) {
            buffer.writeByte(RealTimeDataType.FUELCELL.getId());
            buffer.writeBytes(fuelCellData.encode());
        }
        if (engineData != null) {
            buffer.writeByte(RealTimeDataType.ENGINE.getId());
            buffer.writeBytes(engineData.encode());
        }
        if (locationData != null) {
            buffer.writeByte(RealTimeDataType.LOCATION.getId());
            buffer.writeBytes(locationData.encode());
        }
        if (extremeData != null) {
            buffer.writeByte(RealTimeDataType.EXTREME.getId());
            buffer.writeBytes(extremeData.encode());
        }
        if (alarmData != null) {
            buffer.writeByte(RealTimeDataType.ALARM.getId());
            buffer.writeBytes(alarmData.encode());
        }
        if (subsystemVoltageCount > 0 && subsystemVoltageDatas != null) {
            buffer.writeByte(RealTimeDataType.VOLTAGE.getId());
            buffer.writeByte(subsystemVoltageCount);
            for (int i = 0; i < subsystemVoltageCount; i++) {
                buffer.writeBytes(subsystemVoltageDatas.get(i).encode());
            }
        }
        if (subsystemTemperatureCount > 0 && subsystemTemperatureDatas != null) {
            buffer.writeByte(RealTimeDataType.TEMPERATURE.getId());
            buffer.writeByte(subsystemTemperatureCount);
            for (int i = 0; i < subsystemTemperatureCount; i++) {
                buffer.writeBytes(subsystemTemperatureDatas.get(i).encode());
            }
        }
        return buffer;
    }

    /**
     * 解析不同的车辆类型数据
     *
     * @param byteBuf
     * @param type
     */
    private void decodeByType(ByteBuf byteBuf, RealTime realtimeData) {
        RealTimeDataType type = RealTimeDataType.valuesOf(byteBuf.readUnsignedByte());
        switch (type) {
            case VEHICLE: {
                IStatus<VehicleData> status = type.getStatus();
                realtimeData.setVehicleData(status.decode(byteBuf.readSlice(20)));
                break;
            }
            case DRIVEMOTOR: {
                IStatus<DriveMotorData> status = type.getStatus();
                realtimeData.setDriveMotorCount(byteBuf.readUnsignedByte());
                List<DriveMotorData> motorDatas = new ArrayList<>();
                if (realtimeData.getDriveMotorCount() > 0 && realtimeData.getDriveMotorCount() <= 253) {
                    for (int i = 0; i < realtimeData.getDriveMotorCount(); i++) {
                        motorDatas.add(status.decode(byteBuf));
                    }
                } else {
                    LOGGER.error("decodeByType {} count:{}", type.getDesc(), realtimeData.getDriveMotorCount());
                }
                realtimeData.setDriveMotorDatas(motorDatas);
                break;
            }
            case FUELCELL: {
                IStatus<FuelCellData> status = type.getStatus();
                realtimeData.setFuelCellData(status.decode(byteBuf));
                break;
            }
            case ENGINE: {
                IStatus<EngineData> status = type.getStatus();
                realtimeData.setEngineData(status.decode(byteBuf));
                break;
            }
            case LOCATION: {
                IStatus<LocationData> status = type.getStatus();
                realtimeData.setLocationData(status.decode(byteBuf));
                break;
            }
            case EXTREME: {
                IStatus<ExtremeData> status = type.getStatus();
                realtimeData.setExtremeData(status.decode(byteBuf));
                break;
            }
            case ALARM: {
                IStatus<AlarmData> status = type.getStatus();
                realtimeData.setAlarmData(status.decode(byteBuf));
                break;
            }
            case ALLPARAMETERS: {
                IStatus<AllParameters> status = type.getStatus();
                realtimeData.setAllParameters(status.decode(byteBuf));
                break;
            }
            case ENERGYSTORAGE: {
                IStatus<EnergyStorage> status = type.getStatus();
                realtimeData.setEnergyStorage(status.decode(byteBuf));
                break;
            }
            case BATTERYBODYVOLTAGE: {
                IStatus<BatteryBodyVoltage> status = type.getStatus();
                realtimeData.setBatteryBodyVoltage(status.decode(byteBuf));
                break;
            }
            case BATTERYBODYCURRENT: {
                IStatus<BatteryBodyTemperature> status = type.getStatus();
                realtimeData.setBatteryBodyTemperature(status.decode(byteBuf));
                break;
            }
            case BATTERYBODYRUNSTATUS: {
                IStatus<BatteryBodyRunStatus> status = type.getStatus();
                realtimeData.setBatteryBodyRunStatus(status.decode(byteBuf));
                break;
            }
            case BATTERYBODYCAPACITY: {
                IStatus<BatteryBodyCapacity> status = type.getStatus();
                realtimeData.setBatteryBodyCapacity(status.decode(byteBuf));
                break;
            }
            case SLAVECOMMUNICATIONSTATUS: {
                IStatus<SlaveCommunicationStatus> status = type.getStatus();
                realtimeData.setSlaveCommunicationStatus(status.decode(byteBuf));
                break;
            }
            case ALLFAULT: {
                IStatus<AllFault> status = type.getStatus();
                realtimeData.setAllFault(status.decode(byteBuf));
                break;
            }
            case EMS2PCS: {
                IStatus<Ems2Pcs> status = type.getStatus();
                realtimeData.setEms2Pcs(status.decode(byteBuf));
                break;
            }
            case VOLTAGE: {
                IStatus<SubsystemVoltageData> status = type.getStatus();
                realtimeData.setSubsystemVoltageCount(byteBuf.readUnsignedByte());
                List<SubsystemVoltageData> voltageData = new ArrayList<>();
                if (realtimeData.getSubsystemVoltageCount() > 0 && realtimeData.getSubsystemVoltageCount() <= 250) {
                    for (int i = 0; i < realtimeData.getSubsystemVoltageCount(); i++) {
                        byteBuf.markReaderIndex();
                        byteBuf.skipBytes(9);
                        short j = byteBuf.readUnsignedByte();
                        byteBuf.resetReaderIndex();
                        voltageData.add(status.decode(byteBuf.readSlice(10 + j * 2)));
                    }
                } else {
                    LOGGER.error("decodeByType {} count:{}", type.getDesc(), realtimeData.getDriveMotorCount());
                }
                realtimeData.setSubsystemVoltageDatas(voltageData);
                break;
            }
            case TEMPERATURE: {
                IStatus<SubsystemTemperatureData> status = type.getStatus();
                realtimeData.setSubsystemTemperatureCount(byteBuf.readUnsignedByte());
                List<SubsystemTemperatureData> temperatureData = new ArrayList<>();
                if (realtimeData.getSubsystemTemperatureCount() > 0 && realtimeData.getSubsystemTemperatureCount() <= 250) {
                    for (int i = 0; i < realtimeData.getSubsystemTemperatureCount(); i++) {
                        byteBuf.markReaderIndex();
                        byteBuf.skipBytes(1);
                        int j = byteBuf.readUnsignedShort();
                        byteBuf.resetReaderIndex();
                        temperatureData.add(status.decode(byteBuf.readSlice(3 + j * 1)));
                    }
                } else {
                    LOGGER.error("decodeByType {} count:{}", type.getDesc(), realtimeData.getDriveMotorCount());
                }
                realtimeData.setSubsystemTemperatureDatas(temperatureData);
                break;
            }
            default:
                LOGGER.warn("decode RealtimeData error");
        }
    }

    public VehicleData getVehicleData() {
        return vehicleData;
    }

    public void setVehicleData(VehicleData vehicleData) {
        this.vehicleData = vehicleData;
    }

    public Short getDriveMotorCount() {
        return driveMotorCount;
    }

    public void setDriveMotorCount(Short driveMotorCount) {
        this.driveMotorCount = driveMotorCount;
    }

    public List<DriveMotorData> getDriveMotorDatas() {
        return driveMotorDatas;
    }

    public void setDriveMotorDatas(List<DriveMotorData> driveMotorDatas) {
        this.driveMotorDatas = driveMotorDatas;
    }

    public FuelCellData getFuelCellData() {
        return fuelCellData;
    }

    public void setFuelCellData(FuelCellData fuelCellData) {
        this.fuelCellData = fuelCellData;
    }

    public EngineData getEngineData() {
        return engineData;
    }

    public void setEngineData(EngineData engineData) {
        this.engineData = engineData;
    }

    public LocationData getLocationData() {
        return locationData;
    }

    public void setLocationData(LocationData locationData) {
        this.locationData = locationData;
    }

    public ExtremeData getExtremeData() {
        return extremeData;
    }

    public void setExtremeData(ExtremeData extremeData) {
        this.extremeData = extremeData;
    }

    public AlarmData getAlarmData() {
        return alarmData;
    }

    public void setAlarmData(AlarmData alarmData) {
        this.alarmData = alarmData;
    }

    public Short getSubsystemVoltageCount() {
        return subsystemVoltageCount;
    }

    public void setSubsystemVoltageCount(Short subsystemVoltageCount) {
        this.subsystemVoltageCount = subsystemVoltageCount;
    }

    public List<SubsystemVoltageData> getSubsystemVoltageDatas() {
        return subsystemVoltageDatas;
    }

    public void setSubsystemVoltageDatas(List<SubsystemVoltageData> subsystemVoltageDatas) {
        this.subsystemVoltageDatas = subsystemVoltageDatas;
    }

    public Short getSubsystemTemperatureCount() {
        return subsystemTemperatureCount;
    }

    public void setSubsystemTemperatureCount(Short subsystemTemperatureCount) {
        this.subsystemTemperatureCount = subsystemTemperatureCount;
    }

    public List<SubsystemTemperatureData> getSubsystemTemperatureDatas() {
        return subsystemTemperatureDatas;
    }

    public void setSubsystemTemperatureDatas(List<SubsystemTemperatureData> subsystemTemperatureDatas) {
        this.subsystemTemperatureDatas = subsystemTemperatureDatas;
    }

    public BeanTime getBeanTime() {
        return beanTime;
    }

    public void setBeanTime(BeanTime beanTime) {
        this.beanTime = beanTime;
    }

    public AllParameters getAllParameters() {
        return allParameters;
    }

    public void setAllParameters(AllParameters allParameters) {
        this.allParameters = allParameters;
    }

    public EnergyStorage getEnergyStorage() {
        return energyStorage;
    }

    public void setEnergyStorage(EnergyStorage energyStorage) {
        this.energyStorage = energyStorage;
    }

    public BatteryBodyVoltage getBatteryBodyVoltage() {
        return batteryBodyVoltage;
    }

    public void setBatteryBodyVoltage(BatteryBodyVoltage batteryBodyVoltage) {
        this.batteryBodyVoltage = batteryBodyVoltage;
    }

    public BatteryBodyTemperature getBatteryBodyTemperature() {
        return batteryBodyTemperature;
    }

    public void setBatteryBodyTemperature(BatteryBodyTemperature batteryBodyTemperature) {
        this.batteryBodyTemperature = batteryBodyTemperature;
    }

    public BatteryBodyRunStatus getBatteryBodyRunStatus() {
        return batteryBodyRunStatus;
    }

    public void setBatteryBodyRunStatus(BatteryBodyRunStatus batteryBodyRunStatus) {
        this.batteryBodyRunStatus = batteryBodyRunStatus;
    }

    public BatteryBodyCapacity getBatteryBodyCapacity() {
        return batteryBodyCapacity;
    }

    public void setBatteryBodyCapacity(BatteryBodyCapacity batteryBodyCapacity) {
        this.batteryBodyCapacity = batteryBodyCapacity;
    }

    public SlaveCommunicationStatus getSlaveCommunicationStatus() {
        return slaveCommunicationStatus;
    }

    public void setSlaveCommunicationStatus(SlaveCommunicationStatus slaveCommunicationStatus) {
        this.slaveCommunicationStatus = slaveCommunicationStatus;
    }

    public AllFault getAllFault() {
        return allFault;
    }

    public void setAllFault(AllFault allFault) {
        this.allFault = allFault;
    }

    public Ems2Pcs getEms2Pcs() {
        return ems2Pcs;
    }

    public void setEms2Pcs(Ems2Pcs ems2Pcs) {
        this.ems2Pcs = ems2Pcs;
    }
}
