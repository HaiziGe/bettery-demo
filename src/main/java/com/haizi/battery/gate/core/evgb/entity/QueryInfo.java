package com.haizi.battery.gate.core.evgb.entity;

import cn.hutool.core.util.HexUtil;
import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.base.IStatus;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;
import java.util.List;

/**
 * 车辆登出
 */
@SuppressWarnings("all")
public class QueryInfo implements IStatus {

    private static final BeanTime producer = new BeanTime();

    //车辆登出时间
    private BeanTime beanTime;

    //车辆登出流水号
    private Short count = 1;

    //车辆登出流水号
    private Short query = 17;

    private String info;

    @Override
    public QueryInfo decode(ByteBuf byteBuf) throws BaseException {
        QueryInfo queryInfo = new QueryInfo();
        BeanTime beanTime = producer.decode(byteBuf);
        queryInfo.setBeanTime(beanTime);
        queryInfo.setCount(byteBuf.readUnsignedByte());
        queryInfo.setQuery(byteBuf.readUnsignedByte());
        byte[] bytes = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(bytes);
        queryInfo.setInfo(HexUtil.encodeHexStr(bytes));
        return queryInfo;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.writeBytes(beanTime.encode());
        buffer.writeByte(count);
        buffer.writeByte(query);
        return buffer;
    }

    public BeanTime getBeanTime() {
        return beanTime;
    }

    public void setBeanTime(BeanTime beanTime) {
        this.beanTime = beanTime;
    }

    public Short getCount() {
        return count;
    }

    public void setCount(Short count) {
        this.count = count;
    }

    public Short getQuery() {
        return query;
    }

    public void setQuery(Short query) {
        this.query = query;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }


}
