package com.haizi.battery.gate.core.evgb.entity;

import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.base.IStatus;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class UpdateBms implements IStatus {

    private BeanTime beanTime = new BeanTime(System.currentTimeMillis());

    private Short commandId = 8;

    private String dialPoint;

    private String user;

    private String passwd;

    private String ip;

    private Short port;

    private String manufacturer;

    private String hardwareVersion;

    private String softwareVersion;

    private String url;

    private Short timeLimit;

    @Override
    public Object decode(ByteBuf datas) throws BaseException {
        return null;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.writeBytes(getBeanTime().encode());
        buffer.writeByte(getCommandId());
        StringBuilder builder = new StringBuilder();
        builder.append(getDialPoint());
        builder.append(';');
        builder.append(getUser());
        builder.append(';');
        builder.append(getPasswd());
        builder.append(';');
        buffer.writeBytes(builder.toString().getBytes());
        buffer.writeByte(0);
        buffer.writeByte(0);
        String[] split = this.getIp().split("\\.");
        buffer.writeByte(Integer.parseInt(split[0]));
        buffer.writeByte(Integer.parseInt(split[1]));
        buffer.writeByte(Integer.parseInt(split[2]));
        buffer.writeByte(Integer.parseInt(split[3]));
        buffer.writeBytes(";".getBytes());
        buffer.writeShort(getPort());
        builder.setLength(0);
        builder.append(';');
        builder.append(getManufacturer());
        builder.append(';');
        builder.append(getHardwareVersion());
        builder.append(';');
        builder.append(getSoftwareVersion());
        builder.append(';');
        builder.append(getUrl());
        builder.append(';');
        buffer.writeBytes(builder.toString().getBytes());
        buffer.writeShort(getTimeLimit());

        return buffer;
    }

    public BeanTime getBeanTime() {
        return beanTime;
    }

    public void setBeanTime(BeanTime beanTime) {
        this.beanTime = beanTime;
    }

    public Short getCommandId() {
        return commandId;
    }

    public void setCommandId(Short commandId) {
        this.commandId = commandId;
    }

    public String getDialPoint() {
        return dialPoint;
    }

    public void setDialPoint(String dialPoint) {
        this.dialPoint = dialPoint;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Short getPort() {
        return port;
    }

    public void setPort(Short port) {
        this.port = port;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getHardwareVersion() {
        return hardwareVersion;
    }

    public void setHardwareVersion(String hardwareVersion) {
        this.hardwareVersion = hardwareVersion;
    }

    public String getSoftwareVersion() {
        return softwareVersion;
    }

    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Short getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(Short timeLimit) {
        this.timeLimit = timeLimit;
    }
}
