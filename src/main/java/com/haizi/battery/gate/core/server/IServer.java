package com.haizi.battery.gate.core.server;

/**
 * created by dyy
 */
public interface IServer {
    void start();

    void stop();
}
