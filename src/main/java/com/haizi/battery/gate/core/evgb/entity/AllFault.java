package com.haizi.battery.gate.core.evgb.entity;

import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.base.IStatus;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class AllFault implements IStatus {

    private List<String> faultList = new ArrayList<>();
    /**
     * 报警等级
     */
    private Byte byte1;

    /**
     * 报警等级
     */
    private Byte byte2;

    /**
     * 报警等级
     */
    private Byte byte3;

    /**
     * 报警等级
     */
    private Byte byte4;

    /**
     * 报警等级
     */
    private Byte byte5;

    private String[] faltArray = {"过温", "短路", "电网过流", "负载过流", "预充失败", "升压失败", "BMS故障",
            "风机1故障", "风机2故障", "硬件过流", "电池过流", "电网过压", "逆变过压", "电网欠压", "逆变欠压",
            "逆变过流", "电网过频", "电网欠频", "电网锁相故障", "直流母线过压", "直流母线欠压",
            "电池过压", "电池欠压", "电网瞬时过压", "电网瞬时欠压", "逆变瞬时过流", "直流母线瞬时过压", "直流母线瞬时欠压",
            "电池瞬时过压", "电池瞬时欠压", "电网瞬时过流", "负载瞬时过流", "电池瞬时过流"};


    @Override
    public AllFault decode(ByteBuf byteBuf) throws BaseException {
        AllFault fault = new AllFault();
        fault.setByte1(byteBuf.readByte());
        fault.setByte2(byteBuf.readByte());
        fault.setByte3(byteBuf.readByte());
        fault.setByte4(byteBuf.readByte());
        fault.setByte5(byteBuf.readByte());

        for (int i = 0; i < 8; i++) {
            if (((fault.getByte1() >> i) & 0x1) > 0) {
                fault.faultList.add(faltArray[i]);
            }
        }
        for (int i = 0; i < 8; i++) {
            if (((fault.getByte2() >> i) & 0x1) > 0) {
                fault.faultList.add(faltArray[8+i]);
            }
        }
        for (int i = 0; i < 8; i++) {
            if (((fault.getByte3() >> i) & 0x1) > 0) {
                fault.faultList.add(faltArray[16+i]);
            }
        }
        for (int i = 0; i < 8; i++) {
            if (((fault.getByte4() >> i) & 0x1) > 0) {
                fault.faultList.add(faltArray[24+i]);
            }
        }
        if (((fault.getByte5() >> 0) & 0x01) > 0) {
            fault.faultList.add(faltArray[32]);
        }
        if (fault.faultList.isEmpty()) {
            fault.faultList.add("无");
        }
        return fault;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.order(ByteOrder.BIG_ENDIAN);
        return buffer;
    }

    public Byte getByte1() {
        return byte1;
    }

    public void setByte1(Byte byte1) {
        this.byte1 = byte1;
    }

    public Byte getByte2() {
        return byte2;
    }

    public void setByte2(Byte byte2) {
        this.byte2 = byte2;
    }

    public Byte getByte3() {
        return byte3;
    }

    public void setByte3(Byte byte3) {
        this.byte3 = byte3;
    }

    public Byte getByte4() {
        return byte4;
    }

    public void setByte4(Byte byte4) {
        this.byte4 = byte4;
    }

    public Byte getByte5() {
        return byte5;
    }

    public void setByte5(Byte byte5) {
        this.byte5 = byte5;
    }

    public List<String> getFaultList() {
        return faultList;
    }

    public void setFaultList(List<String> faultList) {
        this.faultList = faultList;
    }


    public Short getAlarm() {
        Short alarm = 1;
        if (byte1 == 0 && byte2 == 0 && byte3 == 0
                && byte4 == 0 && byte5 == 0) {
            alarm = 0;
        }
        return alarm;
    }
}
