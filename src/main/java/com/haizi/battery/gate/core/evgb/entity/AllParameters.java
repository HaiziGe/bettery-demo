package com.haizi.battery.gate.core.evgb.entity;

import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.base.IStatus;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class AllParameters implements IStatus {

    /**
     * 上电时间 单位：秒，精度 1 秒
     */
    private Integer powerOnTime;

    /**
     * 运行时间 单位：秒，精度 1 秒
     */
    private Integer runTime;

    /**
     * 运行状态
     * 1=停机
     * 2=开机自检
     * 3=预工作
     * 4=工作
     * 5=故障
     * 6=故障闭锁
     * 其他值=未定义
     */
    private Short runStatus;

    /**
     * 运行模式
     * 1=并网 PQ 模式
     * 2=离网 VF 模式
     * 其他值=未定义
     */
    private Short runModel;

    /**
     * 电网频率 单位：Hz，精度：0.1
     */
    private Integer frequency;

    /**
     * 温度 1 单位：℃，精度：0.1
     */
    private Integer temperature1;

    /**
     * 温度 2 单位：℃，精度：0.1
     */
    private Integer temperature2;

    /**
     * 温度 3 单位：℃，精度：0.1
     */
    private Integer temperature3;

    /**
     * 温度 4 单位：℃，精度：0.1
     */
    private Integer temperature4;

    /**
     * 交流有功 单位：W，精度：1W
     */
    private Integer alternatingActivePower;

    /**
     * 交流无功 单位：Var，精度：1Var
     */
    private Integer alternatingReactivePower;

    /**
     * 负载有功 单位：W，精度：1W
     */
    private Integer loadActivePower;

    /**
     * 负载无功 单位：Var，精度：1Var
     */
    private Integer loadReactivePower;

    /**
     * 逆变有功 单位：W，精度：1W
     */
    private Integer inverterActivePower;

    /**
     * 逆变无功 单位：Var，精度：1Var
     */
    private Integer inverterReactivePower;

    @Override
    public AllParameters decode(ByteBuf byteBuf) throws BaseException {
        AllParameters allParameters = new AllParameters();
        allParameters.setPowerOnTime((int) byteBuf.readUnsignedShort());
        allParameters.setRunTime((int) byteBuf.readUnsignedShort());
        allParameters.setRunStatus((short) byteBuf.readUnsignedByte());
        allParameters.setRunModel((short) byteBuf.readUnsignedByte());
        allParameters.setFrequency((int) byteBuf.readUnsignedShort());
        allParameters.setTemperature1((int) byteBuf.readUnsignedShort());
        allParameters.setTemperature2((int) byteBuf.readUnsignedShort());
        allParameters.setTemperature3((int) byteBuf.readUnsignedShort());
        allParameters.setTemperature4((int) byteBuf.readUnsignedShort());
        allParameters.setAlternatingActivePower((int) byteBuf.readUnsignedShort());
        allParameters.setAlternatingReactivePower((int) byteBuf.readUnsignedShort());
        allParameters.setLoadActivePower((int) byteBuf.readUnsignedShort());
        allParameters.setLoadReactivePower((int) byteBuf.readUnsignedShort());
        allParameters.setInverterActivePower((int) byteBuf.readUnsignedShort());
        allParameters.setInverterReactivePower((int) byteBuf.readUnsignedShort());
        return allParameters;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.order(ByteOrder.BIG_ENDIAN);
        return buffer;
    }

    public Integer getPowerOnTime() {
        return powerOnTime;
    }

    public void setPowerOnTime(Integer powerOnTime) {
        this.powerOnTime = powerOnTime;
    }

    public Integer getRunTime() {
        return runTime;
    }

    public void setRunTime(Integer runTime) {
        this.runTime = runTime;
    }

    public Short getRunStatus() {
        return runStatus;
    }

    public void setRunStatus(Short runStatus) {
        this.runStatus = runStatus;
    }

    public Short getRunModel() {
        return runModel;
    }

    public void setRunModel(Short runModel) {
        this.runModel = runModel;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getTemperature1() {
        return temperature1;
    }

    public void setTemperature1(Integer temperature1) {
        this.temperature1 = temperature1;
    }

    public Integer getTemperature2() {
        return temperature2;
    }

    public void setTemperature2(Integer temperature2) {
        this.temperature2 = temperature2;
    }

    public Integer getTemperature3() {
        return temperature3;
    }

    public void setTemperature3(Integer temperature3) {
        this.temperature3 = temperature3;
    }

    public Integer getTemperature4() {
        return temperature4;
    }

    public void setTemperature4(Integer temperature4) {
        this.temperature4 = temperature4;
    }

    public Integer getAlternatingActivePower() {
        return alternatingActivePower;
    }

    public void setAlternatingActivePower(Integer alternatingActivePower) {
        this.alternatingActivePower = alternatingActivePower;
    }

    public Integer getAlternatingReactivePower() {
        return alternatingReactivePower;
    }

    public void setAlternatingReactivePower(Integer alternatingReactivePower) {
        this.alternatingReactivePower = alternatingReactivePower;
    }

    public Integer getLoadActivePower() {
        return loadActivePower;
    }

    public void setLoadActivePower(Integer loadActivePower) {
        this.loadActivePower = loadActivePower;
    }

    public Integer getLoadReactivePower() {
        return loadReactivePower;
    }

    public void setLoadReactivePower(Integer loadReactivePower) {
        this.loadReactivePower = loadReactivePower;
    }

    public Integer getInverterActivePower() {
        return inverterActivePower;
    }

    public void setInverterActivePower(Integer inverterActivePower) {
        this.inverterActivePower = inverterActivePower;
    }

    public Integer getInverterReactivePower() {
        return inverterReactivePower;
    }

    public void setInverterReactivePower(Integer inverterReactivePower) {
        this.inverterReactivePower = inverterReactivePower;
    }

}
