package com.haizi.battery.gate.core.evgb.entity;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class BatteryCapacity {

    /**
     * 电池箱体编号 有效值范围：1 ～ 255
     */
    private Short bodyNo;

    /**
     * 设计容量
     */
    private Long designCapacity;

    /**
     * 充满容量
     */
    private Long fullCapacity;

    /**
     * 剩余容量
     */
    private Long remainingCapacity;

    public Short getBodyNo() {
        return bodyNo;
    }

    public void setBodyNo(Short bodyNo) {
        this.bodyNo = bodyNo;
    }

    public Long getDesignCapacity() {
        return designCapacity;
    }

    public void setDesignCapacity(Long designCapacity) {
        this.designCapacity = designCapacity;
    }

    public Long getFullCapacity() {
        return fullCapacity;
    }

    public void setFullCapacity(Long fullCapacity) {
        this.fullCapacity = fullCapacity;
    }

    public Long getRemainingCapacity() {
        return remainingCapacity;
    }

    public void setRemainingCapacity(Long remainingCapacity) {
        this.remainingCapacity = remainingCapacity;
    }
}
