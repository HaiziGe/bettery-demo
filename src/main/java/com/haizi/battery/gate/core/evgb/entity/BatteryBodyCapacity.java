package com.haizi.battery.gate.core.evgb.entity;

import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.base.IStatus;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class BatteryBodyCapacity implements IStatus {

    /**
     * 电池箱体个数
     */
    private Short bodyCount;

    /**
     * 运行状态列表
     */
    private List<BatteryCapacity> batteryCapacityList;

    @Override
    public BatteryBodyCapacity decode(ByteBuf byteBuf) throws BaseException {
        BatteryBodyCapacity batteryBodyCapacity = new BatteryBodyCapacity();
        batteryBodyCapacity.setBodyCount(byteBuf.readUnsignedByte());
        List<BatteryCapacity> batteryRunStatusList =new ArrayList<>();
        for (int i = 0; i < batteryBodyCapacity.getBodyCount(); i++) {
            BatteryCapacity batteryCapacity = new BatteryCapacity();
            batteryCapacity.setBodyNo(byteBuf.readUnsignedByte());
            batteryCapacity.setDesignCapacity(byteBuf.readUnsignedInt());
            batteryCapacity.setFullCapacity(byteBuf.readUnsignedInt());
            batteryCapacity.setRemainingCapacity(byteBuf.readUnsignedInt());
            batteryRunStatusList.add(batteryCapacity);
        }
        batteryBodyCapacity.setBatteryCapacityList(batteryRunStatusList);
        return batteryBodyCapacity;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.order(ByteOrder.BIG_ENDIAN);
        return buffer;
    }

    public Short getBodyCount() {
        return bodyCount;
    }

    public void setBodyCount(Short bodyCount) {
        this.bodyCount = bodyCount;
    }

    public List<BatteryCapacity> getBatteryCapacityList() {
        return batteryCapacityList;
    }

    public void setBatteryCapacityList(List<BatteryCapacity> batteryCapacityList) {
        this.batteryCapacityList = batteryCapacityList;
    }
}
