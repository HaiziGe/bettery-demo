package com.haizi.battery.gate.core.evgb.entity;

import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.entity.constant.GateConstants;
import com.haizi.battery.gate.core.base.IStatus;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * 车辆登入
 */
@SuppressWarnings("all")
public class VehicleLogin implements IStatus {

    private static final BeanTime producer = new BeanTime();

    //车辆登入时间
    private BeanTime beanTime;

    //车辆登入流水号
    private Integer serialNum;

    //ICCID 20位
    private String iccid;

    //电池箱体总个数
    private Short count;

    //最大可充电功率
    private Integer maxChargePower;

    //最大可放电功率
    private Integer maxDisChargePower;


    @Override
    public VehicleLogin decode(ByteBuf byteBuf) throws BaseException {
        VehicleLogin vehicleLogin = new VehicleLogin();
        BeanTime beanTime = producer.decode(byteBuf);
        vehicleLogin.setBeanTime(beanTime);
        vehicleLogin.setSerialNum(byteBuf.readUnsignedShort());
        vehicleLogin.setIccid(byteBuf.readSlice(20).toString(Charset.forName(GateConstants.UTF_8)));
        vehicleLogin.setCount(byteBuf.readUnsignedByte());
        vehicleLogin.setMaxChargePower(byteBuf.readUnsignedShort());
        vehicleLogin.setMaxDisChargePower(byteBuf.readUnsignedShort());
        return vehicleLogin;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        if(iccid.length()!=20){
            throw new BaseException("iccid length must be 20");
        }
        ByteBuf buffer = Unpooled.buffer();
//        buffer.order(ByteOrder.BIG_ENDIAN);
//        buffer.writeBytes(beanTime.encode());
//        buffer.writeShort(serialNum);
//        buffer.writeBytes(iccid.getBytes(Charset.forName(GateConstants.UTF_8)));
//        if(codes.isEmpty()){
//            buffer.writeByte(0);
//            buffer.writeByte(0);
//        }else{
//            buffer.writeByte(codes.size());
//            buffer.writeByte(codes.get(0).getBytes().length);
//            for (String code : codes) {
//                buffer.writeBytes(code.getBytes(Charset.forName(GateConstants.UTF_8)));
//            }
//        }
        return buffer;
    }

    public BeanTime getBeanTime() {
        return beanTime;
    }

    public void setBeanTime(BeanTime beanTime) {
        this.beanTime = beanTime;
    }

    public Integer getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(Integer serialNum) {
        this.serialNum = serialNum;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public Short getCount() {
        return count;
    }

    public void setCount(Short count) {
        this.count = count;
    }


    public Integer getMaxChargePower() {
        return maxChargePower;
    }

    public void setMaxChargePower(Integer maxChargePower) {
        this.maxChargePower = maxChargePower;
    }

    public Integer getMaxDisChargePower() {
        return maxDisChargePower;
    }

    public void setMaxDisChargePower(Integer maxDisChargePower) {
        this.maxDisChargePower = maxDisChargePower;
    }
}
