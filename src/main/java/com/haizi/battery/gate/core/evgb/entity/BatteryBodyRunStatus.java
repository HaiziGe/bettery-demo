package com.haizi.battery.gate.core.evgb.entity;

import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.base.IStatus;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class BatteryBodyRunStatus implements IStatus {

    /**
     * 电池箱体个数
     */
    private Short bodyCount;

    /**
     * 运行状态列表
     */
    private List<BatteryRunStatus> batteryRunStatusList;

    @Override
    public BatteryBodyRunStatus decode(ByteBuf byteBuf) throws BaseException {
        BatteryBodyRunStatus batteryBodyRunStatus = new BatteryBodyRunStatus();
        batteryBodyRunStatus.setBodyCount(byteBuf.readUnsignedByte());
        List<BatteryRunStatus> batteryRunStatusList =new ArrayList<>();
        for (int i = 0; i < batteryBodyRunStatus.getBodyCount(); i++) {
            BatteryRunStatus batteryRunStatus = new BatteryRunStatus();
            batteryRunStatus.setBodyNo(byteBuf.readUnsignedByte());
            batteryRunStatus.setDischargeSatus(byteBuf.readUnsignedByte());
            batteryRunStatus.setChargeSatus(byteBuf.readUnsignedByte());
            batteryRunStatus.setMosTempSatus(byteBuf.readUnsignedByte());
            batteryRunStatus.setEnvTempSatus(byteBuf.readUnsignedByte());
            batteryRunStatus.setCurrent(byteBuf.readUnsignedShort());
            batteryRunStatus.setCellOverVoltageProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setAllOverVoltageProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setFullProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setCellUnderVoltageProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setAllUnderVoltageProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setChargeTempProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setDischargeTempProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setMosOverTempProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setOverTempProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setUnderTempProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setDischargeShortCircuitProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setDischargeOverCurrentProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setChargeOverCurrentProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setEnvHighTempProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setEnvLowTempProtection(byteBuf.readUnsignedByte());
            batteryRunStatus.setDischargeMos(byteBuf.readUnsignedByte());
            batteryRunStatus.setChargeMos(byteBuf.readUnsignedByte());
            batteryRunStatus.setCellectTempFail(byteBuf.readUnsignedByte());
            batteryRunStatus.setCellectVoltageFail(byteBuf.readUnsignedByte());
            batteryRunStatus.setDischargeMos1(byteBuf.readUnsignedByte());
            batteryRunStatus.setChargeMos1(byteBuf.readUnsignedByte());
            batteryRunStatus.setSoc(byteBuf.readUnsignedByte());
            batteryRunStatus.setLoopCount(byteBuf.readUnsignedShort());
            batteryRunStatusList.add(batteryRunStatus);
        }
        batteryBodyRunStatus.setBatteryRunStatusList(batteryRunStatusList);
        return batteryBodyRunStatus;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.order(ByteOrder.BIG_ENDIAN);
        return buffer;
    }

    public Short getBodyCount() {
        return bodyCount;
    }

    public void setBodyCount(Short bodyCount) {
        this.bodyCount = bodyCount;
    }

    public List<BatteryRunStatus> getBatteryRunStatusList() {
        return batteryRunStatusList;
    }

    public void setBatteryRunStatusList(List<BatteryRunStatus> batteryRunStatusList) {
        this.batteryRunStatusList = batteryRunStatusList;
    }

}
