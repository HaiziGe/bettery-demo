package com.haizi.battery.gate.core.evgb.enumtype;

import com.haizi.battery.gate.core.base.IStatus;
import com.haizi.battery.gate.core.evgb.entity.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 实时数据上报中的九组数据
 */
@SuppressWarnings("all")
public enum RealTimeDataType {

    VEHICLE((short)1, "整车数据", new VehicleData()),
    DRIVEMOTOR((short)2,"驱动电机数据", new DriveMotorData()),
    FUELCELL((short)3,"燃料电池数据", new FuelCellData()),
    ENGINE((short)4,"发动机数据", new EngineData()),
    LOCATION((short)5,"车辆位置",new LocationData()),
    EXTREME((short)6,"极值数据",new ExtremeData()),
    ALARM((short)7,"报警数据",new AlarmData()),
    VOLTAGE((short)8,"可充电储能装置电压数据",new SubsystemVoltageData()),
    TEMPERATURE((short)9,"可充电储能装置温度数据", new SubsystemTemperatureData()),

    ALLPARAMETERS((short)128, "整机运行参数", new AllParameters()),
    ENERGYSTORAGE((short)129, "储能装置电压电流数据", new EnergyStorage()),
    BATTERYBODYVOLTAGE((short)130, "电池箱体电压数据", new BatteryBodyVoltage()),
    BATTERYBODYCURRENT((short)131, "电池箱体温度数据", new BatteryBodyTemperature()),
    BATTERYBODYRUNSTATUS((short)132, "电池箱运行状态", new BatteryBodyRunStatus()),
    BATTERYBODYCAPACITY((short)133, "电池箱容量参数", new BatteryBodyCapacity()),
    SLAVECOMMUNICATIONSTATUS((short)134, "从机通信状态", new SlaveCommunicationStatus()),
    ALLFAULT((short)135, "整机故障状态", new AllFault()),
    EMS2PCS((short)136, "EMS-To-PCS参数", new Ems2Pcs()),
    ;

    private Short id;
    private String desc;
    private IStatus status;

    RealTimeDataType(Short id, String desc, IStatus status) {
        this.id = id;
        this.desc = desc;
        this.status = status;
    }

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public IStatus getStatus() {
        return status;
    }

    public void setStatus(IStatus status) {
        this.status = status;
    }

    private static Map<Short, RealTimeDataType> RealTimeDataMap = new HashMap<>();

    private synchronized static void initMap() {
        for (RealTimeDataType enums : RealTimeDataType.values()) {
            RealTimeDataMap.put(enums.id, enums);
        }
    }

    public static RealTimeDataType valuesOf(short id) {
        if (RealTimeDataMap.isEmpty()) {
            initMap();
        }
        return RealTimeDataMap.get(id);
    }
}
