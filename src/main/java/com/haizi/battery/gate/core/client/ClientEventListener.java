package com.haizi.battery.gate.core.client;

/**
 * created by dyy
 */
@SuppressWarnings("all")
public interface ClientEventListener {

    public void onConnected();

    public void onClosed();
}
