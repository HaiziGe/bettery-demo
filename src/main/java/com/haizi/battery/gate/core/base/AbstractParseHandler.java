package com.haizi.battery.gate.core.base;

import com.alibaba.fastjson.JSONObject;
import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.evgb.enumtype.CommandType;
import io.netty.buffer.ByteBuf;

/**
 * 实现顶层方法，子类重写对应方法
 * created by dyy
 */
public class AbstractParseHandler implements IParse {

    @Override
    public JSONObject parseUpJson(CommandType commandType, ByteBuf body) throws BaseException {
        return null;
    }

    @Override
    public JSONObject parseDownJson(CommandType CommandType, ByteBuf body) throws BaseException {
        return null;
    }
}
