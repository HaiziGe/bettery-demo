package com.haizi.battery.gate.core.base;

import com.haizi.battery.entity.gateway.VehicleCache;
import com.haizi.battery.gate.core.evgb.entity.EvGBProtocol;
import com.haizi.battery.gate.core.evgb.enumtype.ResponseType;
import io.netty.channel.Channel;

/**
 * 业务处理类
 * created by dyy
 */
@SuppressWarnings("all")
public interface IHandler {

    Boolean checkLogin(VehicleCache vehicleCache);

    void doBusiness(EvGBProtocol evGBProtocol, Channel channel);

    void doCommonResponse(ResponseType responseType, EvGBProtocol protrocol, Channel channel);
}
