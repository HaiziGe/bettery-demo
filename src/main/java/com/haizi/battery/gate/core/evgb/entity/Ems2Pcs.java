package com.haizi.battery.gate.core.evgb.entity;

import io.netty.util.CharsetUtil;
import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.base.IStatus;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class Ems2Pcs implements IStatus {

    /**
     * 交流电压给定 单位V，精度：0.1，有效值范围：0~2500
     */
    private Integer acVoltageGive;

    /**
     * 交流有功功率给定 单位W，精度：1，有效值范围：-6000~6000
     */
    private Integer acActivePowerGive;

    /**
     * 交流无功功率给定 单位 Var，精度：1，有效值范围：-6000~6000
     */
    private Integer acReactivePowerGive;

    /**
     * 充电截止电压 单位 V，精度：0.1，有效值范围：0~600
     */
    private Integer chargeCutoffVoltage;

    /**
     * 放电截止电压 单位 V，精度：0.1，有效值范围：0~600
     */
    private Integer dischargeCutoffVoltage;

    /**
     * 段 X 起始时间
     */
    private String startTime1;
    /**
     * 段 X 起始时间
     */
    private String endTime1;
    /**
     * 段 X 给定功率
     */
    private Integer power1;
    /**
     * 段 X 起始时间
     */
    private String startTime2;
    /**
     * 段 X 起始时间
     */
    private String endTime2;
    /**
     * 段 X 给定功率
     */
    private Integer power2;
    /**
     * 段 X 起始时间
     */
    private String startTime3;
    /**
     * 段 X 起始时间
     */
    private String endTime3;
    /**
     * 段 X 给定功率
     */
    private Integer power3;
    /**
     * 段 X 起始时间
     */
    private String startTime4;
    /**
     * 段 X 起始时间
     */
    private String endTime4;
    /**
     * 段 X 给定功率
     */
    private Integer power4;
    /**
     * 段 X 起始时间
     */
    private String startTime5;
    /**
     * 段 X 起始时间
     */
    private String endTime5;
    /**
     * 段 X 给定功率
     */
    private Integer power5;
    /**
     * 段 X 起始时间
     */
    private String startTime6;
    /**
     * 段 X 起始时间
     */
    private String endTime6;
    /**
     * 段 X 给定功率
     */
    private Integer power6;

    /**
     * 禁止定时充放
     */
    private Integer noTimingCharge;

    /**
     * 命令控制 0-无动作，1-开机，2-关机
     */
    private Short commandControl;

    private int year;

    private short month;

    private short day;

    private short hours;

    private short minutes;

    private short seconds;

    public Integer getAcVoltageGive() {
        return acVoltageGive;
    }

    public void setAcVoltageGive(Integer acVoltageGive) {
        this.acVoltageGive = acVoltageGive;
    }

    public Integer getAcActivePowerGive() {
        return acActivePowerGive;
    }

    public void setAcActivePowerGive(Integer acActivePowerGive) {
        this.acActivePowerGive = acActivePowerGive;
    }

    public Integer getAcReactivePowerGive() {
        return acReactivePowerGive;
    }

    public void setAcReactivePowerGive(Integer acReactivePowerGive) {
        this.acReactivePowerGive = acReactivePowerGive;
    }

    public Integer getChargeCutoffVoltage() {
        return chargeCutoffVoltage;
    }

    public void setChargeCutoffVoltage(Integer chargeCutoffVoltage) {
        this.chargeCutoffVoltage = chargeCutoffVoltage;
    }

    public Integer getDischargeCutoffVoltage() {
        return dischargeCutoffVoltage;
    }

    public void setDischargeCutoffVoltage(Integer dischargeCutoffVoltage) {
        this.dischargeCutoffVoltage = dischargeCutoffVoltage;
    }

    public String getStartTime1() {
        return startTime1;
    }

    public void setStartTime1(String startTime1) {
        this.startTime1 = startTime1;
    }

    public String getEndTime1() {
        return endTime1;
    }

    public void setEndTime1(String endTime1) {
        this.endTime1 = endTime1;
    }

    public Integer getPower1() {
        return power1;
    }

    public void setPower1(Integer power1) {
        this.power1 = power1;
    }

    public String getStartTime2() {
        return startTime2;
    }

    public void setStartTime2(String startTime2) {
        this.startTime2 = startTime2;
    }

    public String getEndTime2() {
        return endTime2;
    }

    public void setEndTime2(String endTime2) {
        this.endTime2 = endTime2;
    }

    public Integer getPower2() {
        return power2;
    }

    public void setPower2(Integer power2) {
        this.power2 = power2;
    }

    public String getStartTime3() {
        return startTime3;
    }

    public void setStartTime3(String startTime3) {
        this.startTime3 = startTime3;
    }

    public String getEndTime3() {
        return endTime3;
    }

    public void setEndTime3(String endTime3) {
        this.endTime3 = endTime3;
    }

    public Integer getPower3() {
        return power3;
    }

    public void setPower3(Integer power3) {
        this.power3 = power3;
    }

    public String getStartTime4() {
        return startTime4;
    }

    public void setStartTime4(String startTime4) {
        this.startTime4 = startTime4;
    }

    public String getEndTime4() {
        return endTime4;
    }

    public void setEndTime4(String endTime4) {
        this.endTime4 = endTime4;
    }

    public Integer getPower4() {
        return power4;
    }

    public void setPower4(Integer power4) {
        this.power4 = power4;
    }

    public String getStartTime5() {
        return startTime5;
    }

    public void setStartTime5(String startTime5) {
        this.startTime5 = startTime5;
    }

    public String getEndTime5() {
        return endTime5;
    }

    public void setEndTime5(String endTime5) {
        this.endTime5 = endTime5;
    }

    public Integer getPower5() {
        return power5;
    }

    public void setPower5(Integer power5) {
        this.power5 = power5;
    }

    public String getStartTime6() {
        return startTime6;
    }

    public void setStartTime6(String startTime6) {
        this.startTime6 = startTime6;
    }

    public String getEndTime6() {
        return endTime6;
    }

    public void setEndTime6(String endTime6) {
        this.endTime6 = endTime6;
    }

    public Integer getPower6() {
        return power6;
    }

    public void setPower6(Integer power6) {
        this.power6 = power6;
    }

    public Integer getNoTimingCharge() {
        return noTimingCharge;
    }

    public void setNoTimingCharge(Integer noTimingCharge) {
        this.noTimingCharge = noTimingCharge;
    }

    public Short getCommandControl() {
        return commandControl;
    }

    public void setCommandControl(Short commandControl) {
        this.commandControl = commandControl;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public short getMonth() {
        return month;
    }

    public void setMonth(short month) {
        this.month = month;
    }

    public short getDay() {
        return day;
    }

    public void setDay(short day) {
        this.day = day;
    }

    public short getHours() {
        return hours;
    }

    public void setHours(short hours) {
        this.hours = hours;
    }

    public short getMinutes() {
        return minutes;
    }

    public void setMinutes(short minutes) {
        this.minutes = minutes;
    }

    public short getSeconds() {
        return seconds;
    }

    public void setSeconds(short seconds) {
        this.seconds = seconds;
    }


    @Override
    public Ems2Pcs decode(ByteBuf byteBuf) throws BaseException {
        Ems2Pcs ems2Pcs = new Ems2Pcs();
        ems2Pcs.setAcVoltageGive(byteBuf.readUnsignedShort());
        ems2Pcs.setAcActivePowerGive(byteBuf.readUnsignedShort());
        ems2Pcs.setAcReactivePowerGive(byteBuf.readUnsignedShort());
        ems2Pcs.setChargeCutoffVoltage(byteBuf.readUnsignedShort());
        ems2Pcs.setDischargeCutoffVoltage(byteBuf.readUnsignedShort());
        ems2Pcs.setStartTime1(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setEndTime1(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setPower1(byteBuf.readUnsignedShort());
        ems2Pcs.setStartTime2(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setEndTime2(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setPower2(byteBuf.readUnsignedShort());
        ems2Pcs.setStartTime3(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setEndTime3(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setPower3(byteBuf.readUnsignedShort());
        ems2Pcs.setStartTime4(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setEndTime4(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setPower4(byteBuf.readUnsignedShort());
        ems2Pcs.setStartTime5(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setEndTime5(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setPower5(byteBuf.readUnsignedShort());
        ems2Pcs.setStartTime6(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setEndTime6(Integer.toHexString(byteBuf.readUnsignedShort()));
        ems2Pcs.setPower6(byteBuf.readUnsignedShort());
        ems2Pcs.setNoTimingCharge(byteBuf.readUnsignedShort());
        ems2Pcs.setCommandControl(byteBuf.readUnsignedByte());
        ems2Pcs.setYear(byteBuf.readUnsignedShort());
        ems2Pcs.setMonth(byteBuf.readUnsignedByte());
        ems2Pcs.setDay(byteBuf.readUnsignedByte());
        ems2Pcs.setHours(byteBuf.readUnsignedByte());
        ems2Pcs.setMinutes(byteBuf.readUnsignedByte());
        ems2Pcs.setSeconds(byteBuf.readUnsignedByte());
        return ems2Pcs;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.order(ByteOrder.BIG_ENDIAN);
        return buffer;
    }

}
