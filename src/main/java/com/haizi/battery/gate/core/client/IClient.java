package com.haizi.battery.gate.core.client;

/**
 * created by dyy
 */
@SuppressWarnings("all")
public interface IClient {
    public void start();

    public void stop();
}
