package com.haizi.battery.gate.core.evgb.entity;

import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.entity.constant.GateConstants;
import com.haizi.battery.gate.core.base.IStatus;
import com.haizi.battery.utils.ByteUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class SlaveCommunicationStatus implements IStatus {

    /**
     * 从机个数
     */
    private Short slaveCount;

    /**
     * 加热状态
     */
    private Short heatingStatus;

    /**
     * 从机运行状态
     */
    private List<Short> runStatusList;

    @Override
    public SlaveCommunicationStatus decode(ByteBuf byteBuf) throws BaseException {
        SlaveCommunicationStatus slaveCommunicationStatus = new SlaveCommunicationStatus();
        slaveCommunicationStatus.setSlaveCount(byteBuf.readUnsignedByte());
        slaveCommunicationStatus.setHeatingStatus(byteBuf.readUnsignedByte());
        List<Short> runStatusList =new ArrayList<>();
        for (int i = 0; i < slaveCommunicationStatus.getSlaveCount(); i++) {
            runStatusList.add(byteBuf.readUnsignedByte());
        }
        slaveCommunicationStatus.setRunStatusList(runStatusList);
        return slaveCommunicationStatus;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.order(ByteOrder.BIG_ENDIAN);
        return buffer;
    }

    public Short getSlaveCount() {
        return slaveCount;
    }

    public void setSlaveCount(Short slaveCount) {
        this.slaveCount = slaveCount;
    }

    public Short getHeatingStatus() {
        return heatingStatus;
    }

    public void setHeatingStatus(Short heatingStatus) {
        this.heatingStatus = heatingStatus;
    }

    public List<Short> getRunStatusList() {
        return runStatusList;
    }

    public void setRunStatusList(List<Short> runStatusList) {
        this.runStatusList = runStatusList;
    }
}
