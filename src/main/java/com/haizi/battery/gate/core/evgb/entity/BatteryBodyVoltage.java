package com.haizi.battery.gate.core.evgb.entity;

import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.gate.core.base.IStatus;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class BatteryBodyVoltage implements IStatus {

    /**
     * 电池箱体个数
     */
    private Short bodyCount;

    /**
     * 报警通用标志
     */
    private List<BatteryVoltage> batteryVoltageList;

    @Override
    public BatteryBodyVoltage decode(ByteBuf byteBuf) throws BaseException {
        BatteryBodyVoltage batteryBodyVoltage = new BatteryBodyVoltage();
        batteryBodyVoltage.setBodyCount(byteBuf.readUnsignedByte());
        List<BatteryVoltage> batteryVoltageList =new ArrayList<>();
        for (int i = 0; i <batteryBodyVoltage.getBodyCount(); i++) {
            BatteryVoltage batteryVoltage = new BatteryVoltage();
            batteryVoltage.setBodyNo(byteBuf.readUnsignedByte());
            batteryVoltage.setAllVoltage(byteBuf.readUnsignedShort());
            batteryVoltage.setBatteryCount(byteBuf.readUnsignedByte());
            if (batteryVoltage.getBatteryCount() > 0) {
                batteryVoltage.setVoltageList(new ArrayList<>());
            }
            for (int j = 0; j < batteryVoltage.getBatteryCount(); j++) {
                batteryVoltage.getVoltageList().add(byteBuf.readUnsignedShort());
            }
            batteryVoltageList.add(batteryVoltage);
        }
        batteryBodyVoltage.setBatteryVoltageList(batteryVoltageList);
        return batteryBodyVoltage;
    }

    @Override
    public ByteBuf encode() throws BaseException {
        ByteBuf buffer = Unpooled.buffer();
        buffer.order(ByteOrder.BIG_ENDIAN);
        return buffer;
    }

    public Short getBodyCount() {
        return bodyCount;
    }

    public void setBodyCount(Short bodyCount) {
        this.bodyCount = bodyCount;
    }

    public List<BatteryVoltage> getBatteryVoltageList() {
        return batteryVoltageList;
    }

    public void setBatteryVoltageList(List<BatteryVoltage> batteryVoltageList) {
        this.batteryVoltageList = batteryVoltageList;
    }

}
