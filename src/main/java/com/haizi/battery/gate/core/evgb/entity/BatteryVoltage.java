package com.haizi.battery.gate.core.evgb.entity;

import java.util.List;

/**
 * 报警数据
 */
@SuppressWarnings("all")
public class BatteryVoltage {

    /**
     * 电池箱体个数
     */
    private Short bodyNo;

    /**
     * 电池箱体个数
     */
    private Integer allVoltage;

    /**
     * 电池箱体个数
     */
    private Short batteryCount;

    /**
     * 报警通用标志
     */
    private List<Integer> voltageList;

    public Short getBodyNo() {
        return bodyNo;
    }

    public void setBodyNo(Short bodyNo) {
        this.bodyNo = bodyNo;
    }

    public Integer getAllVoltage() {
        return allVoltage;
    }

    public void setAllVoltage(Integer allVoltage) {
        this.allVoltage = allVoltage;
    }

    public Short getBatteryCount() {
        return batteryCount;
    }

    public void setBatteryCount(Short batteryCount) {
        this.batteryCount = batteryCount;
    }

    public List<Integer> getVoltageList() {
        return voltageList;
    }

    public void setVoltageList(List<Integer> voltageList) {
        this.voltageList = voltageList;
    }
}
