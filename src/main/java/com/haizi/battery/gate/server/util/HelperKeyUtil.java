package com.haizi.battery.gate.server.util;

import com.haizi.battery.gate.server.common.CachePrefixEnum;

public class HelperKeyUtil {

    /**
     * 生成内存中车辆缓存固定规则Key
     * @param vin
     * @return
     */
    public static String getKey(String vin) {
        return CachePrefixEnum.VEHICLE_CACHE.getPrefix() + vin;
    }
}
