package com.haizi.battery.gate.server.enumtype;

import com.haizi.battery.gate.core.evgb.entity.*;
import com.haizi.battery.gate.server.handler.*;
import com.haizi.battery.gate.core.base.IStatus;

import java.util.HashMap;
import java.util.Map;

public enum EvGBHandlerType {

    //上行指令
    VEHICLE_LOGIN((short)1, "车辆登入",new VehicleLogin(), VehicleHandler.class),
    VEHICLE_LOGOUT((short)4,"车辆登出",new VehicleLogout(),VehicleHandler.class),
    REALTIME_DATA_REPORTING((short)2,"实时信息上报",new RealTime(), RealTimeDataHandler.class),
    REPLACEMENT_DATA_REPORTING((short)3,"补发信息上报",new RealTime(), RealTimeDataHandler.class),
    PLATFORM_LOGIN((short)5,"平台登入",new PlatformLogin(), PlatformHandler.class), //国家过检才用
    PLATFORM_LOGOUT((short)6,"平台登出",new PlatformLogout(), PlatformHandler.class), //国家过检才用
    HEARTBEAT((short)7,"心跳",null, HeartBeatHandler.class),
    TERMINAL_CHECK_TIME((short)8,"终端校时",null, CheckTimeHandler.class),

    //下行指令
    QUERY_COMMAND((short)128,"平台参数查询命令",new QueryInfo(), QueryHandler.class),
    SET_COMMAND((short)129,"平台设置命令",null,null),
    REMOTE_CONTROL((short)130,"终端控制命令",null,null),
    ;

    private Short id;
    private String desc;
    private IStatus status;
    private Class handler;

    EvGBHandlerType(Short id, String desc, IStatus status, Class handler) {
        this.id = id;
        this.desc = desc;
        this.status = status;
        this.handler = handler;
    }

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public IStatus getStatus() {
        return status;
    }

    public void setStatus(IStatus status) {
        this.status = status;
    }

    public Class getHandler() {
        return handler;
    }

    public void setHandler(Class handler) {
        this.handler = handler;
    }

    private static Map<Short, EvGBHandlerType> handlerTypeMap = new HashMap<>();

    private synchronized static void initMap() {
        for (EvGBHandlerType enums : EvGBHandlerType.values()) {
            handlerTypeMap.put(enums.id, enums);
        }
    }

    public static EvGBHandlerType valuesOf(Short id) {
        if (handlerTypeMap.isEmpty()) {
            initMap();
        }
        return handlerTypeMap.get(id);
    }

}
