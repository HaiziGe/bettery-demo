package com.haizi.battery.gate.server.netty;

import com.haizi.battery.gate.core.base.EvGBDecode;
import com.haizi.battery.gate.core.server.netty.AbstractNettyServer;
import com.haizi.battery.gate.server.config.EvGBProperties;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 基于GB32960数据接入网关服务
 * 开放端口8111
 * created by dyy
 */
public class EvGBServer extends AbstractNettyServer {

    @Autowired
    private EvGBHandler evGBHandler;

    @Autowired
    private EvGBParseHandler evGBParseHandler;

    @Autowired
    private EvGBProperties evGBProperties;

    @Override
    public ChannelInitializer<SocketChannel> getChannelInitializer() {
        return new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast(new LengthFieldBasedFrameDecoder(evGBProperties.getMaxFrameLength(),
                                evGBProperties.getLengthFieldOffset(), evGBProperties.getLengthFieldLength(),
                                evGBProperties.getLengthAdjustment(), evGBProperties.getInitialBytesToStrip(),
                                evGBProperties.getFailFast()))
                        .addLast(new EvGBDecode(evGBParseHandler, Boolean.TRUE, Boolean.TRUE))
                        .addLast(new IdleStateHandler(evGBProperties.getTimeout(), evGBProperties.getTimeout(), 0))
                        .addLast(evGBHandler);
            }
        };
    }

    @PostConstruct
    @Override
    public void start() {
        super.start();
    }

    @PreDestroy
    @Override
    public void stop() {
        super.stop();
    }
}
