package com.haizi.battery.gate.server.handler;

import com.alibaba.fastjson.JSON;
import com.haizi.battery.gate.core.base.AbstractBusinessHandler;
import com.haizi.battery.gate.core.evgb.entity.EvGBProtocol;
import com.haizi.battery.gate.core.evgb.entity.QueryInfo;
import com.haizi.battery.gate.core.evgb.entity.VehicleLogin;
import com.haizi.battery.gate.core.evgb.entity.VehicleLogout;
import com.haizi.battery.gate.core.evgb.enumtype.ResponseType;
import com.haizi.battery.gate.server.common.CommonCache;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 查询命令
 * created by HaiziGe
 */
@Service
@SuppressWarnings("all")
public class QueryHandler extends AbstractBusinessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueryHandler.class);

    @Override
    public void doBusiness(EvGBProtocol protrocol, Channel channel) {
        // 读取到返回信息
        QueryInfo queryInfo = protrocol.getBody().getJson().toJavaObject(QueryInfo.class);
        // todo 把返回信息扔到map中去
        CommonCache.queryMap.put(queryInfo.getBeanTime().formatTime() + queryInfo.getQuery(), queryInfo.getInfo());
    }

    public void sendQuery(EvGBProtocol protocol, Channel channel) {
        channel.writeAndFlush(protocol.encode());
        LOGGER.debug("{} {} 响应{}",protocol.getVin(), protocol.getCommandType().getDesc(), JSON.toJSONString(protocol));
    }
}
