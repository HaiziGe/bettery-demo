package com.haizi.battery.gate.server.handler;

import com.alibaba.fastjson.JSON;
import com.haizi.battery.gate.core.base.AbstractBusinessHandler;
import com.haizi.battery.gate.core.evgb.entity.EvGBProtocol;
import com.haizi.battery.gate.core.evgb.enumtype.ResponseType;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 终端校时处理器。
 * 需要给终端响应
 * created by dyy
 */
@Service
@SuppressWarnings("all")
public class UpdateHandler extends AbstractBusinessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateHandler.class);

    /**
     * 响应当前服务器时间戳
     * @param protocol
     * @param channel
     */
    @Override
    public void doBusiness(EvGBProtocol protocol, Channel channel) {
        channel.writeAndFlush(protocol.encode());
        LOGGER.debug("{} {} 响应{}",protocol.getVin(), protocol.getCommandType().getDesc(), JSON.toJSONString(protocol));
    }

}
