package com.haizi.battery.gate.server.handler;

import com.alibaba.fastjson.JSONObject;
import com.haizi.battery.gate.core.evgb.entity.DataBody;
import com.haizi.battery.gate.core.evgb.entity.EvGBProtocol;
import com.haizi.battery.gate.core.evgb.entity.RealTime;
import com.haizi.battery.gate.server.config.EvGBProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 数据转发处理器
 * created by dyy
 */
@Service
public class ForwardHandler {

    @Autowired
    private EvGBProperties evGBProperties;

    private static final Logger LOGGER = LoggerFactory.getLogger(ForwardHandler.class);

    /**
     * 转发数据到Debug
     * @param protocol
     */
    public void sendToDebug(EvGBProtocol protocol){
        protocol.setGatewayForwardTime(System.currentTimeMillis());
        LOGGER.debug("debug     {}   ", JSONObject.toJSONString(protocol));
    }

    /**
     * 转发数据到Dispatcher
     * @param protocol
     */
    public void sendToDispatcher(EvGBProtocol protocol){
        protocol.setGatewayForwardTime(System.currentTimeMillis());
        // LOGGER.debug("Dispatcher     {}   ", JSONObject.toJSONString(protocol));
        DataBody body = protocol.getBody();
        JSONObject.parseObject(body.getJson().toJSONString(), RealTime.class);
    }

}
