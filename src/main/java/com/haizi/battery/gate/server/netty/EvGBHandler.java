package com.haizi.battery.gate.server.netty;

import com.alibaba.fastjson.JSONObject;
import com.haizi.battery.entity.battery.RealTimeData;
import com.haizi.battery.gate.core.evgb.entity.EvGBProtocol;
import com.haizi.battery.gate.core.handler.AbstractNettyHandler;
import com.haizi.battery.gate.server.common.CommonCache;
import com.haizi.battery.gate.server.handler.BusinessHandler;
import com.haizi.battery.gate.server.util.HelperKeyUtil;
import com.haizi.battery.service.battery.RealTimeDataService;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Netty核心处理器
 * created by dyy
 */
@Service
@ChannelHandler.Sharable
public class EvGBHandler extends AbstractNettyHandler {

    private final static Logger LOGGER = LoggerFactory.getLogger(EvGBHandler.class);

    @Autowired
    private BusinessHandler businessHandler;

    @Autowired
    private RealTimeDataService realTimeDataService;

    @Override
    public void doLogic(ChannelHandlerContext ctx, EvGBProtocol protocol) {
        LOGGER.debug("parse protocol:{}", JSONObject.toJSONString(protocol));
        if(!protocol.getBcc() || !protocol.getBegin()){
            LOGGER.warn("{} invalid data packet",protocol.getVin());
            return;
        }
        businessHandler.doBusiness(protocol,ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception{
        Channel channel = ctx.channel();
        String vin = CommonCache.channelVinMap.remove(channel);
        if(StringUtils.isNotBlank(vin)){
            CommonCache.vinChannelMap.remove(vin);
            CommonCache.vehicleCacheMap.remove(HelperKeyUtil.getKey(vin));
            RealTimeData realTimeData = new RealTimeData();
            realTimeData.setVin(vin);
            realTimeData.setOnline((short) 1);
            realTimeDataService.updateByVin(realTimeData);
        }
        super.channelInactive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        super.exceptionCaught(ctx,cause);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) {
        Channel channel = ctx.channel();
        if(evt instanceof IdleStateEvent){
            IdleState state = ((IdleStateEvent)evt).state();
            if(state == IdleState.READER_IDLE){
                channel.close();
            }
        }
    }
}
