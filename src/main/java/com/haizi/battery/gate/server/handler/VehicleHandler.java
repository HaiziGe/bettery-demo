package com.haizi.battery.gate.server.handler;

import com.haizi.battery.entity.battery.RealTimeData;
import com.haizi.battery.gate.core.base.AbstractBusinessHandler;
import com.haizi.battery.gate.core.evgb.entity.EvGBProtocol;
import com.haizi.battery.gate.core.evgb.entity.VehicleLogin;
import com.haizi.battery.gate.core.evgb.entity.VehicleLogout;
import com.haizi.battery.gate.core.evgb.enumtype.ResponseType;
import com.haizi.battery.gate.server.common.CommonCache;
import com.haizi.battery.service.battery.RealTimeDataService;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 车辆登入与车辆登出处理器
 * 需要给出终端对应响应
 * created by dyy
 */
@Service
@SuppressWarnings("all")
public class VehicleHandler extends AbstractBusinessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleHandler.class);

    @Autowired
    private ForwardHandler forwardHandler;

    @Autowired
    private RealTimeDataService realTimeDataService;

    @Override
    public void doBusiness(EvGBProtocol protrocol, Channel channel) {
        switch (protrocol.getCommandType()){
            case VEHICLE_LOGIN: {
                this.doVehicleLogin(protrocol,channel);
                break;
            }
            case VEHICLE_LOGOUT:{
                this.doVehicleLogout(protrocol,channel);
                break;
            }
            default:
                break;
        }
    }

    /**
     * 车辆登入
     * @param protrocol
     * @param channel
     */
    private void doVehicleLogin(EvGBProtocol protrocol, Channel channel) {
        VehicleLogin vehicleLogin = protrocol.getBody().getJson().toJavaObject(VehicleLogin.class);
//        VehicleCache vehicleCache = protrocol.getVehicleCache();
//        if (vehicleCache == null) {
//            vehicleCache = new VehicleCache();
//        }
//        String redisKey = HelperKeyUtil.getKey(protrocol.getVin());
//        vehicleCache.setLastLoginTime(vehicleLogin.getBeanTime().formatTime());
//        vehicleCache.setLastLoginSerialNum(vehicleLogin.getSerialNum());
//        vehicleCache.setLogin(Boolean.TRUE);
////        redisHandler.set(LibraryType.VEHICLE,redisKey, JSONObject.toJSONString(vehicleCache));
//        CommonCache.vehicleCacheMap.put(redisKey,vehicleCache);
        CommonCache.vinChannelMap.put(protrocol.getVin(),channel);
        CommonCache.channelVinMap.put(channel,protrocol.getVin());
//        forwardHandler.sendToDispatcher(protrocol);
        doCommonResponse(ResponseType.SUCCESS,protrocol,channel);
    }

    /**
     * 车辆登出
     * @param protrocol
     * @param channel
     */
    private void doVehicleLogout(EvGBProtocol protrocol, Channel channel) {
        VehicleLogout vehicleLogout = protrocol.getBody().getJson().toJavaObject(VehicleLogout.class);
//        VehicleCache vehicleCache = protrocol.getVehicleCache();
//        String redisKey = HelperKeyUtil.getKey(protrocol.getVin());
//        vehicleCache.setLastLogoutTime(vehicleLogout.getBeanTime().formatTime());
//        vehicleCache.setLastLogoutSerialNum(vehicleLogout.getSerialNum());
//        vehicleCache.setLogin(Boolean.FALSE);
//        redisHandler.set(LibraryType.VEHICLE,redisKey, JSONObject.toJSONString(vehicleCache));
//        CommonCache.vehicleCacheMap.remove(redisKey);
        CommonCache.vinChannelMap.remove(protrocol.getVin());
        CommonCache.channelVinMap.remove(channel);
//        forwardHandler.sendToDispatcher(protrocol);
        doCommonResponse(ResponseType.SUCCESS,protrocol,channel);

        RealTimeData realTimeData = new RealTimeData();
        realTimeData.setVin(protrocol.getVin());
        realTimeData.setOnline((short) 1);
        realTimeDataService.updateByVin(realTimeData);

        channel.close();

    }
}
