package com.haizi.battery.mapper.battery;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haizi.battery.entity.battery.Battery;


/**
 * 电池
 *
 * @author HaiziGe
 * @date 2020-07-12 13:50:02
 */
public interface BatteryMapper extends BaseMapper<Battery> {


}
