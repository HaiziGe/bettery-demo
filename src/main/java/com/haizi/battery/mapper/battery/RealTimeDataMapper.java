package com.haizi.battery.mapper.battery;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haizi.battery.entity.battery.RealTimeData;

import java.util.Map;

/**
 * 储能站
 *
 * @author HaiziGe
 * @date 2020-07-27 16:19:04
 */
public interface RealTimeDataMapper extends BaseMapper<RealTimeData> {

    Map<String, String> getCountNow();
}
