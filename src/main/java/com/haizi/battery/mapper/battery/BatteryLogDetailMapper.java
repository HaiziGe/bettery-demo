package com.haizi.battery.mapper.battery;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haizi.battery.entity.battery.BatteryLogDetail;
import com.haizi.battery.entity.battery.RealTimeData;

/**
 * 储能站
 *
 * @author HaiziGe
 * @date 2020-07-27 16:19:04
 */
public interface BatteryLogDetailMapper extends BaseMapper<BatteryLogDetail> {

}
