package com.haizi.battery.mapper.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haizi.battery.entity.base.SysDict;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author lihaodong
 * @since 2019-05-17
 */

public interface SysDictMapper extends BaseMapper<SysDict> {

}
