package com.haizi.battery.mapper.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haizi.battery.entity.base.SysDept;

import java.util.List;

/**
 * <p>
 * 部门管理 Mapper 接口
 * </p>
 *
 * @author lihaodong
 * @since 2019-04-21
 */

public interface SysDeptMapper extends BaseMapper<SysDept> {

    List<Integer> getThisLevelChildrenDeptList(Integer deptId);
}
