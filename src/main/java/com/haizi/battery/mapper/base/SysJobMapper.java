package com.haizi.battery.mapper.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haizi.battery.entity.base.SysJob;

/**
 * <p>
 * 岗位管理 Mapper 接口
 * </p>
 *
 * @author lihaodong
 * @since 2019-05-01
 */
public interface SysJobMapper extends BaseMapper<SysJob> {

}
