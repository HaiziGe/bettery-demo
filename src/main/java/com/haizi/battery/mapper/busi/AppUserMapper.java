package com.haizi.battery.mapper.busi;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haizi.battery.entity.busi.bo.AppUser;

/**
 * 微信用户表
 *
 * @author wzh
 * @date 2019-08-13 19:34:16
 */
public interface AppUserMapper extends BaseMapper<AppUser> {

    Integer getAppUserBySession(String sessionKey);
}
