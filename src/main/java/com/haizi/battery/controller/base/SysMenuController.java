package com.haizi.battery.controller.base;

import com.haizi.battery.config.log.SysLog;
import com.haizi.battery.entity.base.SysMenu;
import com.haizi.battery.entity.base.dto.MenuDTO;
import com.haizi.battery.security.PreUser;
import com.haizi.battery.security.User;
import com.haizi.battery.security.util.SecurityUtil;
import com.haizi.battery.service.base.ISysMenuService;
import com.haizi.battery.service.base.ISysUserService;
import com.haizi.battery.utils.PreUtil;
import com.haizi.battery.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 菜单权限表 前端控制器
 * </p>
 *
 * @author lihaodong
 * @since 2019-04-21
 */
@RestController
@RequestMapping("/menu")
public class SysMenuController {

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private ISysUserService userService;

    /**
     * 添加菜单
     *
     * @param menu
     * @return
     */
    @PreAuthorize("hasAuthority('sys:menu:add')")
    @SysLog(descrption = "添加菜单")
    @PostMapping
    public R save(@RequestBody SysMenu menu) {
        return R.ok(menuService.save(menu));
    }

    /**
     * 获取菜单树
     *
     * @return
     */
    @GetMapping
    public R getMenuTree() {
        PreUser securityUser = SecurityUtil.getPreUser();
        return R.ok(menuService.selectMenuTree(securityUser.getUserId()));
    }


    /**
     * 获取所有菜单
     *
     * @return
     */
    @GetMapping("/getMenus")
    public R getMenus() {
        return R.ok(menuService.selectMenuTree(0));
    }

    /**
     * 修改菜单
     *
     * @param menuDto
     * @return
     */
    @PreAuthorize("hasAuthority('sys:menu:update')")
    @SysLog(descrption = "修改菜单")
    @PutMapping
    public R updateMenu(@RequestBody MenuDTO menuDto) {
        return R.ok(menuService.updateMenuById(menuDto));
    }

    /**
     * 根据id删除菜单
     *
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('sys:menu:delete')")
    @SysLog(descrption = "删除菜单")
    @DeleteMapping("/{id}")
    public R deleteMenu(@PathVariable("id") Integer id) {
        return menuService.removeMenuById(id);
    }

    /**
     * 获取路由
     *
     * @return
     */
    @GetMapping("/getRouters")
    public R getRouters() {
        User user = SecurityUtil.getUser();
        Map<String, Object> result = new HashMap<>(4);
        result.put("menuVoList", PreUtil.buildMenus(menuService.selectMenuTree(user.getUserId())));
        result.put("permissionSet", userService.findPermsByUserId(user.getUserId()));
        return R.ok(result);
    }

}

