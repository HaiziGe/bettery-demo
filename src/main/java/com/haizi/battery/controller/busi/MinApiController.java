package com.haizi.battery.controller.busi;

import com.haizi.battery.config.interceptor.AppApi;
import com.haizi.battery.config.interceptor.NoAuth;
import com.haizi.battery.security.util.SecurityUtil;
import com.haizi.battery.service.busi.AppUserService;
import com.haizi.battery.utils.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author HaiziGe
 * @version 1.0
 * @Classname MinApiController
 * @Description 小程序API接口
 * @date 2019-07-20 12:38
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api")
@AppApi
public class MinApiController {

    private final AppUserService appUserService;

    /**
     * 用户信息
     *
     */
    @GetMapping("/userInfo")
    public R getUserInfo() {
        Integer userId = SecurityUtil.getCurrentAppUserId();
        return R.ok(appUserService.getUserInfoById(userId));
    }

    /**
     * 免授权接口测试
     */
    @GetMapping("/noAuth")
    @NoAuth
    public R noAuthTest() {
        return R.ok("我是一个不用授权的接口~");
    }

}
