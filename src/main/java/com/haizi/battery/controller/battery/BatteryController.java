package com.haizi.battery.controller.battery;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haizi.battery.entity.battery.Battery;
import com.haizi.battery.gate.server.handler.UpdateHandler;
import com.haizi.battery.service.battery.BatteryService;
import com.haizi.battery.utils.R;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 电池
 * </p>
 *
 * @author HaiziGe
 * @date 2020-07-12 13:50:02
 */
@RestController
@AllArgsConstructor
@RequestMapping("battery")
public class BatteryController {

    private final BatteryService batteryService;

    @PostMapping
    public R save(@RequestBody Battery battery) {
        return R.ok(batteryService.save(battery));
    }

    @DeleteMapping("/{batteryId}")
    public R delete(@PathVariable("batteryId") Integer batteryId) {
        return R.ok(batteryService.removeById(batteryId));
    }

    @PutMapping
    public R update(@RequestBody Battery battery) {
        return R.ok(batteryService.updateById(battery));
    }

    @GetMapping
    public R page(Page page, @RequestParam(defaultValue = "") String batteryNo) {
        return R.ok(batteryService.selectDeviceList(page, batteryNo));
    }

    /**
     * 获取实时数据
     *
     * @param deviceId
     *
     */
    @GetMapping("/realTime/{deviceId}")
    public R getRealTimeData(@PathVariable Integer deviceId) {
        return R.ok(batteryService.getRealTimeDataById(deviceId));
    }


    /**
     * 获取历史数据
     *
     * @param deviceId
     *
     */
    @GetMapping("/history/{deviceId}")
    public R getHistoryData(@PathVariable Integer deviceId) {
        return R.ok(batteryService.getHistoryDataById(deviceId));
    }

    /**
     * 获取所有数据
     *
     * @param vin
     *
     */
    @GetMapping("/getAllHistory/{vin}")
    public R getAllHistory(Page page,@PathVariable String vin) {
        return R.ok(batteryService.getHistoryData(page,vin));
    }

    @GetMapping("/{batteryId}")
    public R getById(@PathVariable("batteryId") Integer batteryId) {
        return R.ok(batteryService.getById(batteryId));
    }



}

