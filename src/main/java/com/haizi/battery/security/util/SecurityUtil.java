package com.haizi.battery.security.util;

import com.alibaba.fastjson.JSON;
import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.entity.constant.PreConstant;
import com.haizi.battery.security.PreUser;
import com.haizi.battery.security.User;
import com.haizi.battery.utils.R;
import lombok.experimental.UtilityClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @Classname SecurityUtil
 * @Description 安全服务工具类
 * @Author 李号东 lihaodongmail@163.com
 * @Date 2019-05-08 10:12
 * @Version 1.0
 */
@UtilityClass
public class SecurityUtil {

    private final Logger logger = LoggerFactory.getLogger(SecurityUtil.class);

    /**
     * 获取用户
     *
     * @param authentication
     * @return PreUser
     * <p>
     */
    private PreUser getPreUser(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        if (principal instanceof PreUser) {
            return (PreUser) principal;
        }
        return null;
    }

    public void writeJavaScript(R r, HttpServletResponse response) throws IOException {
        response.setStatus(200);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        PrintWriter printWriter = response.getWriter();
        printWriter.write(JSON.toJSONString(r));
        printWriter.flush();
    }

    /**
     * 获取Authentication
     */
    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * @Author 李号东
     * @Description 获取用户
     * @Date 11:29 2019-05-10
     **/
    public PreUser getPreUser() {
        try {
            return (PreUser) getAuthentication().getPrincipal();
        } catch (Exception e) {
//            e.printStackTrace();
            logger.warn(e.getMessage());
            throw new BaseException("登录状态过期", HttpStatus.UNAUTHORIZED.value());
        }
    }

    /**
     * @Author 李号东
     * @Description 获取用户
     * @Date 11:29 2019-05-10
     **/
    public User getUser() {
        try {
            PreUser preUser = (PreUser) getAuthentication().getPrincipal();
            return new User(preUser.getUserId(), preUser.getUsername());
        } catch (Exception e) {
            throw new BaseException("登录状态过期", HttpStatus.UNAUTHORIZED.value());
        }
    }

    /**
     * 获取当前app（小程序）登录的用户信息
     *
     */
    public Integer getCurrentAppUserId() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Object appUserId = request.getAttribute(PreConstant.CURRENT_APP_USER);
        if (null != appUserId) {
            return  (Integer) appUserId;
        } else {
            throw new BaseException("重新登陆授权", HttpStatus.UNAUTHORIZED.value());
        }
    }
}
