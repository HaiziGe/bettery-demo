package com.haizi.battery.security;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author HaiziGe
 * @Description 简单User
 * @Date 2019-07-18 20:11
 * @Param
 * @return
 **/
@Setter
@Getter
public class User {

    private static final long serialVersionUID = 1L;


    private Integer userId;
    private String username;


    public User(Integer userId, String username) {
        this.userId = userId;
        this.username = username;
    }

}
