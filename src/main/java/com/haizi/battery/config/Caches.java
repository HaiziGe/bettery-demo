package com.haizi.battery.config;

/**
 * 缓存
 *
 * @author HaiziGe
 * @date 2019-07-24 10:18:06
 */
public enum Caches {
    /**
     * captcha 验证码缓存
     * key 为请求的随机字符
     * 120s 过期
     */
    captcha(120),
    /**
     * permissions 用户按钮权限缓存
     * key 为userId
     * 2个小时过期
     */
    permissions(7200, 1000),

    /**
     * dataScope 用户角色和数据权限缓存
     * key 为userId
     * 2个小时过期
     */
    dataScope(7200, 1000),
    /**
     * roleAndDs 用户角色和数据权限缓存
     * key 为sessionKey
     * 2个小时过期
     */
    appUserSession(7200, 1000),
    /**
     * 部门信息
     * key 为部门Id
     */
    departments(7200, 100),

    /**
     * 订单状态
     * key 为订单号
     */
    orderStatus(1200, 100)
    ;

    Caches(int ttl) {
        this.ttl = ttl;
    }

    Caches(int ttl, int maxSize) {
        this.ttl = ttl;
        this.maxSize = maxSize;
    }

    private int maxSize = 1000;
    private int ttl = 1000;

    public int getMaxSize() {
        return maxSize;
    }

    public int getTtl() {
        return ttl;
    }
}