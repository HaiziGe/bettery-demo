package com.haizi.battery.config.log;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import com.haizi.battery.entity.base.SysLog;
import com.haizi.battery.security.PreUser;
import com.haizi.battery.security.util.SecurityUtil;
import com.haizi.battery.utils.LogUtil;
import com.haizi.battery.utils.R;
import io.netty.util.concurrent.FastThreadLocal;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;

/**
 * @Classname SysLogAspect
 * @Description 系统日志切面-支持高并发
 * @author HaiziGe ge_haizi@163.com
 * @date 2020-04-14 23:00
 * ①切面注解得到请求数据 -> ②发布监听事件 -> ③异步监听日志入库
 */
@Slf4j
@Aspect
@Component
public class SysLogAspect {


    /**
     * log实体类
     **/
    private FastThreadLocal<SysLog> threadLocalLog = new FastThreadLocal<>();

    /**
     * 事件发布是由ApplicationContext对象管控的，我们发布事件前需要注入ApplicationContext对象调用publishEvent方法完成事件发布
     **/
    @Autowired
    private ApplicationContext applicationContext;

    /***
     * 定义controller切入点拦截规则，拦截SysLog注解的方法
     */
    @Pointcut("@annotation(com.haizi.battery.config.log.SysLog)")
    public void sysLogAspect() {

    }

    /***
     * 拦截控制层的操作日志
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Before(value = "sysLogAspect()")
    public void recordLog(JoinPoint joinPoint) throws Throwable {

        SysLog sysLog = new SysLog();
        // 开始时间
        long beginTime = Instant.now().toEpochMilli();
        sysLog.setBeginTime(beginTime);
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        try {
            PreUser securityUser  = SecurityUtil.getPreUser();
            sysLog.setUserName(securityUser.getUsername());
        } catch (Exception ignored) {
            sysLog.setUserName("APIS");
        }
        sysLog.setActionUrl(URLUtil.getPath(request.getRequestURI()));
        sysLog.setStartTime(LocalDateTime.now());
        sysLog.setRequestIp(ServletUtil.getClientIP(request));
        sysLog.setRequestMethod(request.getMethod());
        sysLog.setUa(request.getHeader("user-agent"));
        //访问目标方法的参数 可动态改变参数值
        Object[] args = joinPoint.getArgs();
        //获取执行的方法名
        sysLog.setActionMethod(joinPoint.getSignature().getName());
        // 类名
        sysLog.setClassPath(joinPoint.getTarget().getClass().getName());
        sysLog.setActionMethod(joinPoint.getSignature().getName());
        sysLog.setFinishTime(LocalDateTime.now());
        // 参数
        sysLog.setParams(Arrays.toString(args));
        sysLog.setDescription(LogUtil.getControllerMethodDescription(joinPoint));
        threadLocalLog.set(sysLog);
    }

    /**
     * 返回通知
     *
     * @param ret
     * @throws Throwable
     */
    @AfterReturning(returning = "ret", pointcut = "sysLogAspect()")
    public void doAfterReturning(Object ret) {
        SysLog sysLog = threadLocalLog.get();
        if (sysLog == null) {
            return;
        }
        long endTime = Instant.now().toEpochMilli();
        sysLog.setConsumingTime(endTime - sysLog.getBeginTime());
        // 处理完请求，返回内容
        R r = Convert.convert(R.class, ret);
        if (r.getCode() == 200) {
            // 正常返回
            sysLog.setType(1);
        } else {
            sysLog.setType(2);
            sysLog.setExDetail(r.getMsg());
        }
        threadLocalLog.remove();
        // 发布事件
        applicationContext.publishEvent(new SysLogEvent(sysLog));
    }

    /**
     * 异常通知
     *
     * @param e
     */
    @AfterThrowing(pointcut = "sysLogAspect()", throwing = "e")
    public void doAfterThrowable(Throwable e) {
        SysLog sysLog = threadLocalLog.get();
        if (sysLog == null) {
            return;
        }
        long endTime = Instant.now().toEpochMilli();
        sysLog.setConsumingTime(endTime - sysLog.getBeginTime());
        // 异常
        sysLog.setType(2);
        // 异常对象
        sysLog.setExDetail(LogUtil.getStackTrace(e));
        // 异常信息
        sysLog.setExDesc(e.getMessage());
        threadLocalLog.remove();
        // 发布事件
        applicationContext.publishEvent(new SysLogEvent(sysLog));
    }

}
