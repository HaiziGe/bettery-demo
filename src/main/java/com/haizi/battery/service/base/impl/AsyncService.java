package com.haizi.battery.service.base.impl;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service
public class AsyncService {

    @Async
    public void executeAsync1(int i) throws InterruptedException {
        Thread.sleep(200);
        System.out.println("异步任务::1      ");

    }

    @Async
    public void executeAsync2() {
        System.out.println("异步任务::2");
    }



    @Async
    public Future<String> theFuture() throws InterruptedException {
        System.out.println("任务执行开始,需要：1000ms");
        for (int i = 0; i < 10; i++) {
            Thread.sleep(100);
            System.out.println("do:" + i);
        }
        System.out.println("完成任务");
        return new AsyncResult<>(Thread.currentThread().getName());
    }

}