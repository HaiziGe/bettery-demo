package com.haizi.battery.service.base.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.entity.base.SysUser;
import com.haizi.battery.entity.base.SysUserRole;
import com.haizi.battery.entity.constant.DataScopeTypeEnum;
import com.haizi.battery.entity.constant.PreConstant;
import com.haizi.battery.entity.base.dto.UserDTO;
import com.haizi.battery.mapper.base.SysUserMapper;
import com.haizi.battery.security.PreUser;
import com.haizi.battery.security.util.JwtUtil;
import com.haizi.battery.service.base.*;
import com.haizi.battery.utils.PreUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author lihaodong
 * @since 2019-04-21
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Autowired
    private ISysUserRoleService userRoleService;

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private ISysJobService jobService;

    @Autowired
    private ISysMenuService menuService;

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private CacheManager cacheManager;


    @Override
    public IPage<SysUser> getUsersWithRolePage(Page page, UserDTO userDTO) {

        if (ObjectUtil.isNotNull(userDTO) && userDTO.getDeptId() != 0) {
            userDTO.setDeptList(deptService.selectDeptIds(userDTO.getDeptId()));
        }
        return baseMapper.getUserVosPage(page, userDTO);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean insertUser(UserDTO userDto) {
        if (userDto.getDsType() == DataScopeTypeEnum.CUSTOMIZE.getType()
                && (userDto.getDeptList()==null || userDto.getDeptList().size() == 0)) {
            return false;
        }
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(userDto, sysUser);
        sysUser.setPassword(PreUtil.encode("123456"));
        baseMapper.insertUser(sysUser);

        // 插入数据权限
        if (sysUser.getDsType() != DataScopeTypeEnum.ALL.getType()) {
            if (sysUser.getDsType() != DataScopeTypeEnum.CUSTOMIZE.getType()) {
                userDto.setDeptList(deptService.getDsScope(sysUser.getDsType(), sysUser.getDeptId()));
            }
            baseMapper.insertUserDept(sysUser.getUserId(),
                    userDto.getDeptList().stream().map(String::valueOf).collect(Collectors.joining(",")));
        }

        // 插入数据权限
        List<SysUserRole> userRoles = getUserRoles(userDto, sysUser.getUserId());
        return userRoleService.saveBatch(userRoles);
    }

    private List<SysUserRole> getUserRoles(UserDTO userDto, Integer userId) {
        return userDto.getRoleList().stream().map(item -> {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setRoleId(item);
            sysUserRole.setUserId(userId);
            return sysUserRole;
        }).collect(Collectors.toList());
    }

    @CacheEvict(value = "dataScope", key = "#userDto.userId")
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateUser(UserDTO userDto) {
        if (userDto.getDsType() == DataScopeTypeEnum.CUSTOMIZE.getType()
                && (userDto.getDeptList()==null || userDto.getDeptList().size() == 0)) {
            return false;
        }
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(userDto, sysUser);
        baseMapper.updateById(sysUser);
        baseMapper.deleteUserDeptByUserId(sysUser.getUserId());
        // 插入数据权限
        if (sysUser.getDsType() != DataScopeTypeEnum.ALL.getType()) {
            if (sysUser.getDsType() != DataScopeTypeEnum.CUSTOMIZE.getType()) {
                userDto.setDeptList(deptService.getDsScope(sysUser.getDsType(), sysUser.getDeptId()));
            }
            baseMapper.insertUserDept(sysUser.getUserId(),
                    userDto.getDeptList().stream().map(String::valueOf).collect(Collectors.joining(",")));
        }
        userRoleService.remove(Wrappers.<SysUserRole>lambdaQuery().eq(SysUserRole::getUserId, sysUser.getUserId()));
        List<SysUserRole> userRoles = getUserRoles(userDto, sysUser.getUserId());
        return userRoleService.saveBatch(userRoles);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean removeUser(Integer userId) {
        // todo 修改删除逻辑
        baseMapper.deleteById(userId);
        return userRoleService.remove(Wrappers.<SysUserRole>lambdaQuery().eq(SysUserRole::getUserId, userId));
    }

    @Override
    public boolean restPass(Integer userId) {
        return baseMapper.updateById(new SysUser().setPassword("123456").setUserId(userId)) > 0;
    }

    @Override
    public SysUser findByUserName(String username) {
        return baseMapper.selectOne(Wrappers.<SysUser>lambdaQuery()
                .select(SysUser::getUserId, SysUser::getUsername, SysUser::getPassword)
                .eq(SysUser::getUsername, username));
    }

    @Override
    public SysUser findByUserInfoName(String username) {
        SysUser sysUser = baseMapper.selectOne(Wrappers.<SysUser>lambdaQuery()
                .select(SysUser::getUserId, SysUser::getUsername, SysUser::getPhone, SysUser::getEmail, SysUser::getPassword, SysUser::getDeptId, SysUser::getJobId, SysUser::getAvatar)
                .eq(SysUser::getUsername, username));
        // 获取部门
        sysUser.setDeptName(deptService.selectDeptNameByDeptId(sysUser.getDeptId()));
        // 获取岗位
        sysUser.setJobName(jobService.selectJobNameByJobId(sysUser.getJobId()));
        return sysUser;
    }

    // 加入缓存
    @Override
    @Cacheable(value = "permissions", key = "#userId")
    public Set<String> findPermsByUserId(Integer userId) {
        return menuService.findPermsByUserId(userId).stream().filter(StringUtils::isNotEmpty).collect(Collectors.toSet());
    }

    @Override
    public Set<String> findRoleIdByUserId(Integer userId) {
        return userRoleService
                .selectUserRoleListByUserId(userId)
                .stream()
                .map(sysUserRole -> "ROLE_" + sysUserRole.getRoleId())
                .collect(Collectors.toSet());
    }

    @Override
    public String login(String username, String password, String captcha, String t) {
        // 验证验证码
        // 从redis中获取之前保存的验证码跟前台传来的验证码进行匹配
        Object kaptcha = cacheManager.getCache("captcha").get(PreConstant.PRE_IMAGE_SESSION_KEY + t, String.class);
        if (kaptcha == null) {
            throw new BaseException("验证码已失效");
        }
        if (!captcha.toLowerCase().equals(kaptcha)) {
            throw new BaseException("验证码错误");
        }
        //用户验证
        Authentication authentication = null;
        try {
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername()去验证用户名和密码，
            // 如果正确，则存储该用户名密码到security 的 context中
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof BadCredentialsException) {
                throw new BaseException("用户名或密码错误", 402);
            } else if (e instanceof DisabledException) {
                throw new BaseException("账户被禁用", 402);
            } else if (e instanceof AccountExpiredException) {
                throw new BaseException("账户过期无法验证", 402);
            } else {
                throw new BaseException("账户被锁定,无法登录", 402);
            }
        }
        //存储认证信息
        SecurityContextHolder.getContext().setAuthentication(authentication);
        //生成token
        PreUser userDetail = (PreUser) authentication.getPrincipal();
        return JwtUtil.generateToken(userDetail);
    }

    @Override
    public boolean updateUserInfo(SysUser sysUser) {
        return baseMapper.updateById(sysUser) > 0;
    }

}
