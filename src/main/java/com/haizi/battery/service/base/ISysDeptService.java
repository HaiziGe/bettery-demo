package com.haizi.battery.service.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haizi.battery.entity.base.SysDept;
import com.haizi.battery.entity.base.dto.DeptDTO;
import com.haizi.battery.entity.base.vo.DeptTreeVo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 部门管理 服务类
 * </p>
 *
 * @author lihaodong
 * @since 2019-04-21
 */
public interface ISysDeptService extends IService<SysDept> {

    /**
     * 保存部门信息
     *
     * @return
     */
    @Override
    boolean save(SysDept entity);

    /**
     * 查询部门信息
     *
     * @return
     */
    List<SysDept> selectDeptList();

    /**
     * 更新部门
     *
     * @param entity
     * @return
     */
    boolean updateDeptById(DeptDTO entity);

    /**
     * 删除部门
     *
     * @param id
     * @return
     */
    @Override
    boolean removeById(Serializable id);

    /**
     * 根据部门id查询部门名称
     *
     * @param deptId
     * @return
     */
    String selectDeptNameByDeptId(int deptId);

    /**
     * 通过此部门id查询于此相关的部门ids
     *
     * @param deptId
     * @return
     */
    List<Integer> selectDeptIds(int deptId);

    /**
     * 获取部门树
     *
     * @return
     */
    List<DeptTreeVo> getDeptTree();


    /**
     * 获取部门的祖级和父级
     * @param deptId
     * @return
     */
    Map<String, Object> getParentDept(Integer deptId);

    /**
     * 根据要求获取数据权限
     * @param dsType 2本级 3本级和子级
     * @param deptId 部门id
     * @return 拥有权限的部门列表
     */
    List<Integer> getDsScope(Integer dsType, Integer deptId);
}
