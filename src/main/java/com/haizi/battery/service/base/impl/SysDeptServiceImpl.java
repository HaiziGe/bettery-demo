package com.haizi.battery.service.base.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haizi.battery.config.Caches;
import com.haizi.battery.config.exception.BaseException;
import com.haizi.battery.entity.base.SysDept;
import com.haizi.battery.entity.base.SysUser;
import com.haizi.battery.entity.constant.DataScopeTypeEnum;
import com.haizi.battery.entity.base.dto.DeptDTO;
import com.haizi.battery.entity.base.vo.DeptTreeVo;
import com.haizi.battery.mapper.base.SysDeptMapper;
import com.haizi.battery.mapper.base.SysUserMapper;
import com.haizi.battery.security.PreUser;
import com.haizi.battery.security.util.SecurityUtil;
import com.haizi.battery.service.base.ISysDeptService;
import com.haizi.battery.utils.PreUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 部门管理 服务实现类
 * </p>
 *
 * @author lihaodong
 * @since 2019-04-21
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private CacheManager cacheManager;

    @Override
    public boolean save(SysDept entity) {
        cacheManager.getCache(Caches.permissions.name()).clear();
        cacheManager.getCache(Caches.dataScope.name()).clear();
        SysDept parentDept = baseMapper.selectById(entity.getParentId());
        if (parentDept == null || !"0".equals(parentDept.getDelFlag())) {
            throw new BaseException("上级部门停用或不存在！");
        }
        entity.setAncestors(parentDept.getAncestors() + "," + entity.getParentId());
        super.save(entity);
        // 新增了一个部门
        sysUserMapper.addNewDept(entity.getDeptId(), entity.getParentId());
        return true;
    }

    @Override
    public List<SysDept> selectDeptList() {
        List<SysDept> depts = baseMapper.selectList(Wrappers.<SysDept>lambdaQuery().select(SysDept::getDeptId, SysDept::getName, SysDept::getParentId, SysDept::getSort, SysDept::getCreateTime).orderByDesc(SysDept::getSort));
        PreUser securityUser = SecurityUtil.getPreUser();
        SysUser sysUser = sysUserMapper.selectById(securityUser.getUserId());
        SysDept sysUserDept = baseMapper.selectById(sysUser.getDeptId());
        List<SysDept> sysDepts = depts.stream()
                .filter(sysDept -> sysDept.getParentId().equals(sysUser.getDeptId()) || ObjectUtil.isNull(sysDept.getParentId()))
                .peek(sysDept -> sysDept.setLevel(0))
                .collect(Collectors.toList());
        PreUtil.findChildren(sysDepts, depts);

        //将本身部门加到最上级
        sysUserDept.setChildren(sysDepts);
        List<SysDept> list=new ArrayList<>();
        list.add(sysUserDept);
        return list;
    }


    @Override
    @CacheEvict(value = "departments", key = "#entity.deptId")
    public boolean updateDeptById(DeptDTO entity) {
        cacheManager.getCache(Caches.permissions.name()).clear();
        cacheManager.getCache(Caches.dataScope.name()).clear();
        SysDept sysDept = new SysDept();
        BeanUtils.copyProperties(entity, sysDept);
        sysDept.setUpdateTime(LocalDateTime.now());
        SysDept parentDept = baseMapper.selectById(entity.getParentId());
        if (parentDept == null || !"0".equals(parentDept.getDelFlag())) {
            throw new BaseException("上级部门停用或不存在！");
        }
        sysDept.setAncestors(parentDept.getAncestors() + "," + entity.getParentId());
        return this.updateById(sysDept);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean removeById(Serializable id) {
        cacheManager.getCache(Caches.permissions.name()).clear();
        cacheManager.getCache(Caches.dataScope.name()).clear();
        // 部门层级删除
        List<Integer> idList = this.list(Wrappers.<SysDept>query().lambda().eq(SysDept::getParentId, id)).stream().map(SysDept::getDeptId).collect(Collectors.toList());
        // 删除自己
        idList.add((Integer) id);
        return super.removeByIds(idList);
    }

    @Override
    @Cacheable(value = "departments", key = "#deptId")
    public String selectDeptNameByDeptId(int deptId) {
        return baseMapper.selectOne(Wrappers.<SysDept>query().lambda().select(SysDept::getName).eq(SysDept::getDeptId, deptId)).getName();
    }

    @Override
    public List<Integer> selectDeptIds(int deptId) {
        SysDept department = this.getDepartment(deptId);
        List<Integer> deptIdList = new ArrayList<>();
        if (department != null) {
            deptIdList.add(department.getDeptId());
            addDeptIdList(deptIdList, department);
        }
        return deptIdList;
    }

    @Override
    public List<DeptTreeVo> getDeptTree() {
        List<SysDept> depts = baseMapper.selectList(Wrappers.<SysDept>query().select("dept_id", "name", "parent_id", "sort", "create_time"));
        List<DeptTreeVo> collect = depts.stream().filter(sysDept -> sysDept.getParentId() == 0 || ObjectUtil.isNull(sysDept.getParentId()))
                .map(sysDept -> {
                    DeptTreeVo deptTreeVo = new DeptTreeVo();
                    deptTreeVo.setId(sysDept.getDeptId());
                    deptTreeVo.setLabel(sysDept.getName());
                    return deptTreeVo;

                }).collect(Collectors.toList());

        PreUtil.findChildren1(collect, depts);
        return collect;
    }

    @Override
    public Map<String, Object> getParentDept(Integer deptId) {
        SysDept sysDept = this.getById(deptId);
        if (sysDept.getParentId() == 0) {
            return null;
        }
        Map<String, Object> result = new HashMap<>(8);
        result.put("parentDeptId", sysDept.getParentId());
        SysDept parentDept = this.getById(sysDept.getParentId());
        result.put("parentDeptName", parentDept.getName());
        if (parentDept.getParentId() != 0) {
            result.put("grandDeptId", parentDept.getParentId());
            SysDept grandDept = this.getById(parentDept.getParentId());
            result.put("grandDeptName", grandDept.getName());
        }
        return result;
    }

    /**
     * 根据要求获取数据权限
     *
     * @param dsType 2本级 3本级和子级
     * @param deptId 部门id
     * @return 拥有权限的部门列表
     */
    @Override
    public List<Integer> getDsScope(Integer dsType, Integer deptId) {
        List<Integer> result = new ArrayList<>();
        if (dsType == DataScopeTypeEnum.THIS_LEVEL.getType()) {
            result.add(deptId);
            return result;
        }
        result = baseMapper.getThisLevelChildrenDeptList(deptId);
        return result;
    }


    /**
     * 根据部门ID获取该部门及其下属部门树
     */
    private SysDept getDepartment(Integer deptId) {
        List<SysDept> departments = baseMapper.selectList(Wrappers.<SysDept>query().select("dept_id", "name", "parent_id", "sort", "create_time"));
        Map<Integer, SysDept> map = departments.stream().collect(
                Collectors.toMap(SysDept::getDeptId, department -> department));

        for (SysDept dept : map.values()) {
            SysDept parent = map.get(dept.getParentId());
            if (parent != null) {
                List<SysDept> children = parent.getChildren() == null ? new ArrayList<>() : parent.getChildren();
                children.add(dept);
                parent.setChildren(children);
            }
        }
        return map.get(deptId);
    }

    private void addDeptIdList(List<Integer> deptIdList, SysDept department) {
        List<SysDept> children = department.getChildren();

        if (children != null) {
            for (SysDept d : children) {
                deptIdList.add(d.getDeptId());
                addDeptIdList(deptIdList, d);
            }
        }
    }


}
