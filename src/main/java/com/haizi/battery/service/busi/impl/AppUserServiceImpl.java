package com.haizi.battery.service.busi.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haizi.battery.entity.busi.bo.AppUser;
import com.haizi.battery.mapper.busi.AppUserMapper;
import com.haizi.battery.service.busi.AppUserService;
import org.springframework.stereotype.Service;

@Service
public class AppUserServiceImpl extends ServiceImpl<AppUserMapper, AppUser> implements AppUserService {

    @Override
    public Integer getAppUserBySession(String sessionKey) {
        return baseMapper.getAppUserBySession(sessionKey);
    }

    @Override
    public AppUser getUserInfoById(Integer userId) {
        return baseMapper.selectById(userId);
    }
}
