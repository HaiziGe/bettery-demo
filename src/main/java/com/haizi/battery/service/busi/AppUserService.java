package com.haizi.battery.service.busi;


import com.haizi.battery.entity.busi.bo.AppUser;

public interface AppUserService {
    Integer getAppUserBySession(String sessionKey);

    AppUser getUserInfoById(Integer userId);
}
