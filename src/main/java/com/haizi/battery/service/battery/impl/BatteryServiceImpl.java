package com.haizi.battery.service.battery.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haizi.battery.entity.base.SysRole;
import com.haizi.battery.entity.battery.Battery;
import com.haizi.battery.entity.battery.BatteryHistory;
import com.haizi.battery.entity.battery.BatteryLogDetail;
import com.haizi.battery.gate.core.evgb.entity.DataBody;
import com.haizi.battery.gate.core.evgb.entity.EvGBProtocol;
import com.haizi.battery.gate.core.evgb.entity.UpdateBms;
import com.haizi.battery.gate.core.evgb.enumtype.ResponseType;
import com.haizi.battery.gate.server.common.CommonCache;
import com.haizi.battery.gate.server.handler.UpdateHandler;
import com.haizi.battery.mapper.battery.BatteryLogDetailMapper;
import com.haizi.battery.mapper.battery.BatteryMapper;
import com.haizi.battery.service.battery.BatteryService;
import com.haizi.battery.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import static com.haizi.battery.gate.core.evgb.enumtype.CommandType.REMOTE_CONTROL;
import static com.haizi.battery.gate.core.evgb.enumtype.CommandType.SET_COMMAND;

/**
 * <p>
 *     电池
 * </p>
 *
 * @author HaiziGe
 * @date 2020-07-12 13:50:02
 */
@Slf4j
@Service
public class BatteryServiceImpl extends ServiceImpl<BatteryMapper, Battery> implements BatteryService {

    @Autowired
    private UpdateHandler updateHandler;

    @Resource
    private BatteryLogDetailMapper batteryLogDetailMapper;

    @Override
    public IPage<Battery> selectDeviceList(Page page, String batteryNo) {

        LambdaQueryWrapper<Battery> lambdaQueryWrapper = Wrappers.<Battery >lambdaQuery();
        if (StringUtils.isNotEmpty(batteryNo)) {
            lambdaQueryWrapper.like(Battery::getBatteryNo, batteryNo);
        }
        IPage<Battery> batteryPage = baseMapper.selectPage(page, lambdaQueryWrapper);
        return batteryPage;
    }

    @Override
    public Battery getRealTimeDataById(Integer batteryId) {
        return this.getById(batteryId);
    }

    @Override
    public BatteryHistory getHistoryDataById(Integer batteryId) {
        return null;
    }

    @Override
    public IPage<BatteryLogDetail> getHistoryData(Page page,String vin) {
        QueryWrapper<BatteryLogDetail> queryWrapper = new QueryWrapper();
        queryWrapper.eq("vin",vin);
        queryWrapper.orderByDesc("DATA_TIME");
        IPage<BatteryLogDetail> batteryLogDetails = batteryLogDetailMapper.selectPage( page,queryWrapper);
        return batteryLogDetails;
    }

    @Override
    public R updateTerminal(String vin, UpdateBms updateBms) {

        log.info(" vin {}", vin);
        EvGBProtocol protocol = new EvGBProtocol();
        protocol.setVin(vin);
        protocol.setCommandType(REMOTE_CONTROL);
        DataBody dataBody = new DataBody(updateBms.encode());
        dataBody.setJson((JSONObject) JSONObject.toJSON(updateBms));
        protocol.setBody(dataBody);
        protocol.setResponseType(ResponseType.COMMAND);
        log.info(" protocol {}", JSON.toJSON(protocol));
        updateHandler.doBusiness(protocol, CommonCache.vinChannelMap.get(vin));

        return R.ok();
    }
}
