package com.haizi.battery.service.battery;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haizi.battery.entity.battery.RealTimeData;
import com.haizi.battery.gate.core.evgb.entity.UpdateBms;
import com.haizi.battery.utils.R;

import java.util.List;
import java.util.Map;


/**
 * 储能站
 *
 * @author HaiziGe
 * @date 2020-07-27 16:19:04
 */
public interface RealTimeDataService extends IService<RealTimeData> {

    @Override
    boolean save(RealTimeData entity);


    void updateByVin(RealTimeData realTimeData);


    List<RealTimeData> getAllForMap();

    R updateTerminal(Integer id, UpdateBms updateBms);

    /**
     * 查询客户端版本
     * @param id
     * @return
     */
    R queryVersion(Integer id);

    Map<String, String> getCountNow();
}
