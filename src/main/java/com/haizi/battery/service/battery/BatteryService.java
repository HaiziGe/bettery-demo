package com.haizi.battery.service.battery;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.haizi.battery.entity.battery.Battery;
import com.haizi.battery.entity.battery.BatteryHistory;
import com.haizi.battery.entity.battery.BatteryLogDetail;
import com.haizi.battery.gate.core.evgb.entity.UpdateBms;
import com.haizi.battery.utils.R;

import java.util.List;


/**
 * 电池
 *
 * @author HaiziGe
 * @date 2020-07-12 13:50:02
 */
public interface BatteryService extends IService<Battery> {

    /**
     * 分页查询岗位列表
     *
     * @param page
     * @param batteryNo
     * @return
     */
    IPage<Battery> selectDeviceList(Page page, String batteryNo);

    /**
     * 获取电池的实时数据
     * @param batteryId id
     *
     */
    Battery getRealTimeDataById(Integer batteryId);

    /**
     * 获取电池的历史数据
     * @param batteryId id
     *
     */
    BatteryHistory getHistoryDataById(Integer batteryId);

    IPage<BatteryLogDetail> getHistoryData(Page page, String vin);

    /**
     * 下发升级指令
     * @param vin 设备id
     * @param updateBms 更新信息
     * @return
     */
    R updateTerminal(String vin, UpdateBms updateBms);
}
